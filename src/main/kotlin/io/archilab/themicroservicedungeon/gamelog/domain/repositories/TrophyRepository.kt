package io.archilab.themicroservicedungeon.gamelog.domain.repositories

import io.archilab.themicroservicedungeon.gamelog.domain.entities.Trophy
import io.archilab.themicroservicedungeon.gamelog.domain.enums.TrophyType

interface TrophyRepository {

    fun store(trophy: Trophy)

    fun retrieve(trophyType: TrophyType) : Trophy?

}