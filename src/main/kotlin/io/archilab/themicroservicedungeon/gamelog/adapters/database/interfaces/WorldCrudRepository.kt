package io.archilab.themicroservicedungeon.gamelog.adapters.database.interfaces

import io.archilab.themicroservicedungeon.gamelog.domain.entities.World
import io.archilab.themicroservicedungeon.gamelog.domain.enums.WorldStatus
import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Repository
import java.util.*

@Repository
interface WorldCrudRepository : CrudRepository<World, UUID> {

    fun findWorldsByStatus(status: WorldStatus) : List<World>

}