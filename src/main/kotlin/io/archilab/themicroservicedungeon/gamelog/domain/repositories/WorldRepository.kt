package io.archilab.themicroservicedungeon.gamelog.domain.repositories

import io.archilab.themicroservicedungeon.gamelog.domain.entities.World
import java.util.*

interface WorldRepository {

    fun getWorldById(worldId: UUID) : World?

    fun getActiveWorld() : World?

    fun save(world: World)

}