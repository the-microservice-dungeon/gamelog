package io.archilab.themicroservicedungeon.gamelog.domain.entities

import org.hibernate.annotations.Type
import java.util.*
import javax.persistence.Column
import javax.persistence.ElementCollection
import javax.persistence.Embeddable

@Embeddable
class Scoreboard() {
    @Column
    @Type(type = "uuid-char")
    lateinit var gameId: UUID
    @Column
    var roundNumber: Int = 0
    @ElementCollection
    lateinit var playerScores: List<PlayerScore>

    constructor(gameId: UUID, roundNumber: Int, playerScores: List<PlayerScore>) : this() {
        this.gameId = gameId
        this.roundNumber = roundNumber
        this.playerScores = playerScores
    }

}