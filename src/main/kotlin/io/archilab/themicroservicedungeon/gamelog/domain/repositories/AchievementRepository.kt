package io.archilab.themicroservicedungeon.gamelog.domain.repositories

import io.archilab.themicroservicedungeon.gamelog.domain.entities.Achievement
import io.archilab.themicroservicedungeon.gamelog.domain.enums.AchievementType

interface AchievementRepository {

    fun store(achievement: Achievement)

    fun retrieve(achievementType: AchievementType): Achievement?

}