package io.archilab.themicroservicedungeon.gamelog.adapters.database.interfaces

import io.archilab.themicroservicedungeon.gamelog.domain.entities.Game
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Repository
import java.time.Instant
import java.util.*

@Repository
interface GameCrudRepository : CrudRepository<Game, UUID> {

    @Query("SELECT g FROM Game g WHERE g.startedAt <= ?1 AND (g.endedAt >= ?1 OR g.endedAt is null) ORDER BY g.endedAt DESC")
    fun findByTimestampBetweenStartedAtAndEndedAt(timestamp: Instant) : List<Game>

    fun findGamesByEndedAtIsNull() : List<Game>

    fun findGamesByEndedAtIsNullOrderByStartedAtDesc() : List<Game>

    fun findGamesByEndedAtIsNotNullOrderByStartedAtDesc() : List<Game>

}