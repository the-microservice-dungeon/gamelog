package io.archilab.themicroservicedungeon.gamelog.adapters.database.repositories

import io.archilab.themicroservicedungeon.gamelog.domain.dtos.ScoreboardTrophiesDto
import io.archilab.themicroservicedungeon.gamelog.domain.entities.*
import io.archilab.themicroservicedungeon.gamelog.domain.mappers.ScoreboardTrophiesDtoMapper
import io.archilab.themicroservicedungeon.gamelog.domain.repositories.GameRepository
import io.archilab.themicroservicedungeon.gamelog.domain.repositories.ScoreboardTrophiesRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Profile
import org.springframework.stereotype.Repository
import java.util.*

@Repository
@Profile("!api-test")
class ScoreboardTrophiesJpaRepository : ScoreboardTrophiesRepository {

    @Autowired
    private lateinit var gameRepository: GameRepository

    @Autowired
    private lateinit var scoreboardTrophiesDtoMapper: ScoreboardTrophiesDtoMapper

    override fun getScoreboardTrophies(): ScoreboardTrophiesDto {
        val mostRecentGame = gameRepository.getLastFinishedGame()
        return scoreboardTrophiesDtoMapper.mapGameScoreboardToScoreboardTrophiesDto(mostRecentGame)
    }

}