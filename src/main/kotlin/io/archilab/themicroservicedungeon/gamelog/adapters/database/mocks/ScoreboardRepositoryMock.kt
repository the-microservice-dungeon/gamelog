package io.archilab.themicroservicedungeon.gamelog.adapters.database.mocks

import io.archilab.themicroservicedungeon.gamelog.domain.dtos.PlayerDto
import io.archilab.themicroservicedungeon.gamelog.domain.repositories.ScoreboardRepository
import io.archilab.themicroservicedungeon.gamelog.domain.dtos.ScoreboardDto
import io.archilab.themicroservicedungeon.gamelog.domain.dtos.ScoreboardEntryDto
import io.archilab.themicroservicedungeon.gamelog.domain.enums.GameStatus
import org.springframework.context.annotation.Profile
import org.springframework.stereotype.Repository
import java.util.UUID
import kotlin.random.Random

@Repository
@Profile("api-test")
class ScoreboardRepositoryMock : ScoreboardRepository {

    private val playerNames = listOf(
        "foo",
        "bar",
        "baz",
        "lorem",
        "ipsum"
    )

    override fun getScoreboard(): ScoreboardDto? {
        val gameId = UUID.randomUUID()
        val scoreboardEntries = generateRandomScoreboardEntries()
        return ScoreboardDto(
            gameId,
            GameStatus.STARTED,
            Random.nextInt(0, 100),
            scoreboardEntries
        )
    }

    private fun generateRandomScoreboardEntries(): List<ScoreboardEntryDto> {
        return playerNames.map {
            ScoreboardEntryDto(
                PlayerDto(
                    UUID.randomUUID(),
                    it
                ),
                generateRandomScore(),
                generateRandomScore(),
                generateRandomScore(),
                generateRandomScore(),
                generateRandomScore()

            )
        }
    }

    fun generateRandomScore() : Int {
        return Random.nextInt(1, 100)
    }

}