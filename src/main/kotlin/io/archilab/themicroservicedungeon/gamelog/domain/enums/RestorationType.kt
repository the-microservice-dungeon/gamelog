package io.archilab.themicroservicedungeon.gamelog.domain.enums

import com.fasterxml.jackson.annotation.JsonValue

enum class RestorationType(@get:JsonValue val type: String) {
    HEALTH("HEALTH"),
    ENERGY("ENERGY")
}