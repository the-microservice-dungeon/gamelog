package io.archilab.themicroservicedungeon.gamelog.domain.events.robot.embedded

data class Inventory(
    val resources: ResourceInventory,
    val storageLevel: Int,
    val usedStorage: Int,
    val maxStorage: Int,
    val full: Boolean
)