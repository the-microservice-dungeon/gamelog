package io.archilab.themicroservicedungeon.gamelog.adapters.kafka.mappers

import TestWithH2AndWithoutKafka
import io.archilab.themicroservicedungeon.gamelog.adapters.kafka.mocks.EventMockFactory
import io.archilab.themicroservicedungeon.gamelog.domain.enums.ResourceType
import io.archilab.themicroservicedungeon.gamelog.domain.enums.WorldStatus
import io.archilab.themicroservicedungeon.gamelog.domain.events.map.GameWorldCreatedEvent
import io.archilab.themicroservicedungeon.gamelog.domain.events.map.GameWorldDeletedEvent
import io.archilab.themicroservicedungeon.gamelog.domain.events.map.GameWorldStatusChangedEvent
import io.archilab.themicroservicedungeon.gamelog.domain.events.map.embedded.GameWorldPlanet
import io.archilab.themicroservicedungeon.gamelog.domain.events.map.embedded.GameWorldResource
import io.archilab.themicroservicedungeon.gamelog.domain.exceptions.MapperUnknownEventTypeException
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows
import java.util.*

class GameWorldEventMapperTest : TestWithH2AndWithoutKafka() {

    private val eventMockFactory = EventMockFactory()
    private val mapper = GameWorldEventMapper()


    private val gameWorldId = UUID.randomUUID()
    private val gameWorldStatus = WorldStatus.ACTIVE
    private val planetId = UUID.randomUUID()
    private val defaultNumber = 0
    private val resourceType = ResourceType.COAL

    private val gameWorldCreatedEvent = eventMockFactory.createGameWorldCreatedEventMock(
        gameWorldId,
        gameWorldStatus.status,
        planetId,
        defaultNumber,
        resourceType.name
    )

    @Test
    fun test_gameWorldCreated() {
        val expected = GameWorldCreatedEvent(
            gameWorldId,
            gameWorldStatus,
            listOf(
                GameWorldPlanet(
                    planetId,
                    defaultNumber,
                    defaultNumber,
                    defaultNumber,
                    GameWorldResource(resourceType, defaultNumber, defaultNumber)
                )
            )
        )
        val result = mapper.mapConsumerRecordToEvent(gameWorldCreatedEvent.type, gameWorldCreatedEvent.timestamp, gameWorldCreatedEvent.payload)
        assertThat(result.javaClass).isEqualTo(GameWorldCreatedEvent::class.java)
        assertThat(result).usingRecursiveComparison().isEqualTo(expected)
    }

    @Test
    fun test_gameWorldStatusChanged() {
        val gameWorldChangedEvent = eventMockFactory.createGameWorldStatusChangedEventMock(gameWorldId, gameWorldStatus.status)
        val expected = GameWorldStatusChangedEvent(gameWorldId, gameWorldStatus)
        val result = mapper.mapConsumerRecordToEvent(gameWorldChangedEvent.type, gameWorldChangedEvent.timestamp, gameWorldChangedEvent.payload)
        assertThat(result.javaClass).isEqualTo(GameWorldStatusChangedEvent::class.java)
        assertThat(result).usingRecursiveComparison().isEqualTo(expected)
    }

    @Test
    fun test_gameWorldDeleted() {
        val gameWorldDeletedEvent = eventMockFactory.createGameWorldDeletedEventMock(gameWorldId)
        val expected = GameWorldDeletedEvent(gameWorldId)
        val result = mapper.mapConsumerRecordToEvent(gameWorldDeletedEvent.type, gameWorldDeletedEvent.timestamp, gameWorldDeletedEvent.payload)
        assertThat(result.javaClass).isEqualTo(GameWorldDeletedEvent::class.java)
        assertThat(result).usingRecursiveComparison().isEqualTo(expected)
    }

    @Test
    fun test_mapConsumerRecordToEvent_exception() {
        assertThrows<MapperUnknownEventTypeException> {
            mapper.mapConsumerRecordToEvent("foobar", gameWorldCreatedEvent.timestamp, gameWorldCreatedEvent.payload)
        }
    }

}