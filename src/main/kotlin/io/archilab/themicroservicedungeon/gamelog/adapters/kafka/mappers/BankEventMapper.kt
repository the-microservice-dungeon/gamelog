package io.archilab.themicroservicedungeon.gamelog.adapters.kafka.mappers

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.kotlin.registerKotlinModule
import io.archilab.themicroservicedungeon.gamelog.domain.exceptions.MapperUnknownEventTypeException
import io.archilab.themicroservicedungeon.gamelog.domain.events.trading.BankAccountClearedEvent
import io.archilab.themicroservicedungeon.gamelog.domain.events.InternalEvent
import io.archilab.themicroservicedungeon.gamelog.domain.events.trading.BankAccountInitializedEvent
import io.archilab.themicroservicedungeon.gamelog.domain.events.trading.BankAccountTransactionBookedEvent
import org.apache.kafka.clients.consumer.ConsumerRecord
import org.springframework.stereotype.Component
import java.time.Instant

@Component
class BankEventMapper : KafkaToInternalEventMapper {

    private val objectMapper = InjectingObjectMapper()

    override fun mapConsumerRecordToEvent(type: String, timestamp: String, payload: ConsumerRecord<String, ByteArray>): InternalEvent {
        val eventClass = when (type) {
            "BankAccountCleared" -> BankAccountClearedEvent::class.java
            "BankAccountInitialized" -> BankAccountInitializedEvent::class.java
            "BankAccountTransactionBooked" -> BankAccountTransactionBookedEvent::class.java
            else -> throw MapperUnknownEventTypeException()
        }
        return objectMapper.mapInjectingValues(mapOf(Pair("timestamp", Instant.parse(timestamp))), payload.value(), eventClass)
    }

}