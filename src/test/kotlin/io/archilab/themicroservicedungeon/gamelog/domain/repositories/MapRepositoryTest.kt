package io.archilab.themicroservicedungeon.gamelog.domain.repositories

import TestWithH2AndWithoutKafka
import io.archilab.themicroservicedungeon.gamelog.adapters.database.repositories.MapJpaRepository
import io.archilab.themicroservicedungeon.gamelog.domain.dtos.MapDto
import io.archilab.themicroservicedungeon.gamelog.domain.dtos.PlanetDto
import io.archilab.themicroservicedungeon.gamelog.domain.dtos.ResourceDto
import io.archilab.themicroservicedungeon.gamelog.domain.dtos.RobotDto
import io.archilab.themicroservicedungeon.gamelog.domain.entities.*
import io.archilab.themicroservicedungeon.gamelog.domain.enums.GameStatus
import io.archilab.themicroservicedungeon.gamelog.domain.enums.ResourceType
import io.archilab.themicroservicedungeon.gamelog.domain.enums.WorldStatus
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.transaction.annotation.Transactional
import java.time.Instant
import java.util.*
import kotlin.random.Random

@Transactional
class MapRepositoryTest : TestWithH2AndWithoutKafka() {

    @Autowired
    private lateinit var mapRepository: MapJpaRepository

    @Autowired
    private lateinit var gameRepository: GameRepository

    @Autowired
    private lateinit var playerRepository: PlayerRepository

    @Autowired
    private lateinit var worldRepository: WorldRepository

    @Autowired
    private lateinit var robotRepository: RobotRepository

    @Test
    fun test_getMap() {
        val gameId = UUID.randomUUID()
        val gameWorldId = UUID.randomUUID()
        val roundNumber = 1
        val game = Game(gameId, gameWorldId, GameStatus.CREATED)
        gameRepository.saveGame(game)

        val worldSize = 5
        val world = World(gameWorldId, WorldStatus.ACTIVE, createPlanets(worldSize))
        worldRepository.save(world)

        val player = Player(UUID.randomUUID(), "foobar")
        player.joinGame(game)
        playerRepository.savePlayer(player)
        gameRepository.saveGame(game)

        game.apply {
            startGame(Instant.now())
            newRound(UUID.randomUUID(), roundNumber, Instant.now())
        }
        gameRepository.saveGame(game)

        val players = mapPlayersToPlayerNames(listOf(player))

        val robots = spawnRobotsOnPlanets(world.planets)

        val worldPlanets = mapPlanetsToPlanetDtosJoiningRobots(world.planets, robots)
        val expected = MapDto(game.id, game.gameStatus, roundNumber, Instant.now(), players, worldPlanets)

        val map = mapRepository.getMap()!!

        assertThat(map).usingRecursiveComparison()
            .ignoringFields("timestamp", "rechargeMultiplicator")
            .ignoringCollectionOrder()
            .isEqualTo(expected)
    }

    private fun spawnRobotsOnPlanets(planets: Set<Planet>): List<Robot> {
        val robots = planets.map {
            Robot(
                UUID.randomUUID(),
                true,
                UUID.randomUUID(),
                100,
                100,
                100,
                100,
                100,
                100,
                100,
                1,
                1,
                1,
                1,
                1,
                1,
                1
            ).apply { spawn(it.id) }
        }
        robots.forEach { robotRepository.save(it) }
        return robots
    }

    private fun createPlanets(worldSize: Int): Set<Planet> {
        val movementDifficulty = 1
        val planets = mutableListOf<Planet>()
        for (x in 0 until worldSize) {
            for (y in 0 until worldSize) {
                planets.add(Planet(UUID.randomUUID(), x, y,
                    movementDifficulty, mapOf(getNewRandomResource())))
            }
        }
        return planets.toSet()
    }

    private fun getNewRandomResource(): Pair<ResourceType, Resource> {
        val amount = Random.nextInt(5, 10) * 10
        val resourceType = ResourceType.values().random()
        return Pair(resourceType, Resource(resourceType, amount, amount))
    }

    private fun mapPlanetsToPlanetDtosJoiningRobots(planets: Set<Planet>, robots: List<Robot>): List<PlanetDto> {
        return planets.map { planet ->
            PlanetDto(planet.id, planet.x, planet.y, planet.movementDifficulty, 1,
                planet.resources.map {
                    with (it.value) {
                        ResourceDto(resourceType, maxAmount, currentAmount)
                    }
                },
                robots.filter { robot -> robot.currentPlanetId == planet.id }.map { robot -> RobotDto(robot.robotId) }
            )
        }
    }

    private fun mapPlayersToPlayerNames(players: List<Player>) : List<String> {
        return players.map { it.name }
    }

}