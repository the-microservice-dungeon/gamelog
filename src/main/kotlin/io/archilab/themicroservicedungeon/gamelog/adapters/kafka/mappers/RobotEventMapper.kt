package io.archilab.themicroservicedungeon.gamelog.adapters.kafka.mappers

import RobotSpawnedEvent
import io.archilab.themicroservicedungeon.gamelog.domain.events.InternalEvent
import io.archilab.themicroservicedungeon.gamelog.domain.events.robot.*
import io.archilab.themicroservicedungeon.gamelog.domain.exceptions.MapperUnknownEventTypeException
import org.apache.kafka.clients.consumer.ConsumerRecord
import org.springframework.stereotype.Component
import java.time.Instant

@Component
class RobotEventMapper : KafkaToInternalEventMapper {

    private val objectMapper = InjectingObjectMapper()

    override fun mapConsumerRecordToEvent(type: String, timestamp: String, payload: ConsumerRecord<String, ByteArray>): InternalEvent {
        val eventClass = when (type) {
            "RobotAttacked" -> RobotAttackedEvent::class.java
            "RobotMoved" -> RobotMovedEvent::class.java
            "RobotRegenerated" -> RobotRegeneratedEvent::class.java
            "RobotResourceMined" -> RobotResourceMinedEvent::class.java
            "RobotResourceRemoved" -> RobotResourceRemovedEvent::class.java
            "RobotRestoredAttributes" -> RobotRestoredAttributesEvent::class.java
            "RobotSpawned" -> RobotSpawnedEvent::class.java
            "RobotUpgraded" -> RobotUpgradedEvent::class.java
            "RobotsRevealed" -> RobotsRevealedEvent::class.java
            else -> throw MapperUnknownEventTypeException()
        }
        return objectMapper.mapInjectingValues(mapOf(Pair("timestamp", Instant.parse(timestamp))), payload.value(), eventClass)
    }

}
