package io.archilab.themicroservicedungeon.gamelog.domain.broker

import RobotSpawnedEvent
import TestWithH2AndWithoutKafka
import io.archilab.themicroservicedungeon.gamelog.domain.entities.Inventory
import io.archilab.themicroservicedungeon.gamelog.domain.entities.MinedResources
import io.archilab.themicroservicedungeon.gamelog.domain.entities.Robot
import io.archilab.themicroservicedungeon.gamelog.domain.enums.ResourceType
import io.archilab.themicroservicedungeon.gamelog.domain.enums.RestorationType
import io.archilab.themicroservicedungeon.gamelog.domain.enums.UpgradeType
import io.archilab.themicroservicedungeon.gamelog.domain.events.robot.*
import io.archilab.themicroservicedungeon.gamelog.domain.events.robot.embedded.*
import io.archilab.themicroservicedungeon.gamelog.domain.mappers.ResourceInventoryToInventoryMapper
import io.archilab.themicroservicedungeon.gamelog.domain.repositories.PlayerRepository
import io.archilab.themicroservicedungeon.gamelog.domain.repositories.RobotRepository
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.system.CapturedOutput
import org.springframework.boot.test.system.OutputCaptureExtension
import org.springframework.transaction.annotation.Transactional
import java.time.Instant
import java.util.*

@Transactional
@ExtendWith(OutputCaptureExtension::class)
class InternalEventListenerTestForRobotEvents : TestWithH2AndWithoutKafka() {

    @Autowired
    private lateinit var internalEventListener: InternalEventListener

    @Autowired
    private lateinit var playerRepository: PlayerRepository

    @Autowired
    private lateinit var robotRepository: RobotRepository

    private val resourceInventoryToInventoryMapper = ResourceInventoryToInventoryMapper()

    private val playerId = UUID.randomUUID()
    private val robotId = UUID.randomUUID()
    private val alive = true
    private val maxHealth = 100
    private val maxEnergy = 100
    private val energyRegeneration = 10
    private val attackDamage = 10
    private val miningSpeed = 10
    private val health = 100
    private val energy = 100
    private val healthLevel = 1
    private val damageLevel = 1
    private val miningSpeedLevel = 1
    private val miningLevel = 1
    private val energyLevel = 1
    private val energyRegenerationLevel = 1
    private val storageLevel = 1

    private val spawnPlanet = Planet(UUID.randomUUID(), UUID.randomUUID(), 1, ResourceType.COAL.name.lowercase())

    private val spawnInventory = Inventory(
        ResourceInventory(1, 2, 3, 4, 5),
        storageLevel, 0, 10, false
    )

    @Test
    fun testRobotSpawnedIntegrationEvent() {
        spawnRobot()
        val expected = Robot(robotId,
            alive,
            playerId,
            maxHealth,
            maxEnergy,
            energyRegeneration,
            attackDamage,
            miningSpeed,
            health,
            energy,
            healthLevel,
            damageLevel,
            miningSpeedLevel,
            miningLevel,
            energyLevel,
            energyRegenerationLevel,
            storageLevel)
        expected.apply {
            spawn(spawnPlanet.planetId)
            inventory = spawnInventory.resources.let { Inventory(it.coal, it.iron, it.gem, it.gold, it.platin) }
        }

        val player = playerRepository.getPlayerById(playerId)
        assertThat(player).isNotNull
        player?.run {
            val robot = robots.firstOrNull()!!
            with(robot) {
                assertThat(this).usingRecursiveComparison().isEqualTo(expected)
                assertThat(currentPlanetId).isEqualTo(spawnPlanet.planetId)
                assertThat(previousPlanetId).isEqualTo(spawnPlanet.planetId)
                assertThat(inventory).usingRecursiveComparison().isEqualTo(
                    spawnInventory.resources.let { Inventory(it.coal, it.iron, it.gem, it.gold, it.platin) }
                )
            }
        }
    }

    @Test
    fun test_robotUpgradedIntegrationEvent_storage() {
        runRobotUpgradedIntegrationEventTestForConfiguration(1, UpgradeType.STORAGE)
    }

    @Test
    fun test_robotUpgradedIntegrationEvent_damage() {
        runRobotUpgradedIntegrationEventTestForConfiguration(1, UpgradeType.DAMAGE)
    }

    @Test
    fun test_robotUpgradedIntegrationEvent_health() {
        runRobotUpgradedIntegrationEventTestForConfiguration(1, UpgradeType.HEALTH)
    }

    @Test
    fun test_robotUpgradedIntegrationEvent_mining() {
        runRobotUpgradedIntegrationEventTestForConfiguration(1, UpgradeType.MINING)
    }

    @Test
    fun test_robotUpgradedIntegrationEvent_miningSpeed() {
        runRobotUpgradedIntegrationEventTestForConfiguration(1, UpgradeType.MINING_SPEED)
    }

    @Test
    fun test_robotUpgradedIntegrationEvent_energyRegeneration() {
        runRobotUpgradedIntegrationEventTestForConfiguration(1, UpgradeType.ENERGY_REGENERATION)
    }

    @Test
    fun test_robotUpgradedIntegrationEvent_energy() {
        runRobotUpgradedIntegrationEventTestForConfiguration(1, UpgradeType.MAXIMUM_ENERGY)
    }

    @Test
    fun test_robotUpgradedIntegrationEvent_downgrade(output: CapturedOutput) {
        runRobotUpgradedIntegrationEventTestForConfiguration(-1, UpgradeType.STORAGE)
        assertThat(output).contains("downgrade")
    }

    fun runRobotUpgradedIntegrationEventTestForConfiguration (upgradeLevels: Int, upgrade: UpgradeType) {
        val robotProperties = spawnRobot()
        val level = when(upgrade) {
            UpgradeType.STORAGE -> robotProperties.inventory.storageLevel + upgradeLevels
            UpgradeType.HEALTH -> robotProperties.healthLevel + upgradeLevels
            UpgradeType.DAMAGE -> robotProperties.damageLevel + upgradeLevels
            UpgradeType.MINING_SPEED -> robotProperties.miningSpeedLevel + upgradeLevels
            UpgradeType.MINING -> robotProperties.miningLevel + upgradeLevels
            UpgradeType.MAXIMUM_ENERGY -> robotProperties.energyLevel + upgradeLevels
            UpgradeType.ENERGY_REGENERATION -> robotProperties.energyRegenerationLevel + upgradeLevels
        }
        val event = generateRobotUpgradedIntegrationEvent(robotProperties, level, upgrade)

        internalEventListener.listenForInternalEvents(event)

        validateLevelUpgrade(upgrade, level)
    }

    @Test
    fun test_robotRestoredAttributesIntegrationEvent_energy() {
        spawnRobot()
        val restorationType = RestorationType.HEALTH
        val availableEnergy = energy + 10
        val availableHealth = health
        val event = RobotRestoredAttributesEvent(robotId, restorationType, availableEnergy, availableHealth)

        internalEventListener.listenForInternalEvents(event)

        validateHealthAndEnergy(availableEnergy, availableHealth)
    }

    @Test
    fun test_robotRestoredAttributesIntegrationEvent_health() {
        spawnRobot()
        val restorationType = RestorationType.HEALTH
        val availableEnergy = energy
        val availableHealth = health + 10
        val event = RobotRestoredAttributesEvent(robotId, restorationType, availableEnergy, availableHealth)

        internalEventListener.listenForInternalEvents(event)

        validateHealthAndEnergy(availableEnergy, availableHealth)
    }

    @Test
    fun test_robotResourceRemovedIntegrationEvent_coal() {
        spawnRobot()
        val inventoryBeforeChange = robotRepository.getRobotById(robotId)?.inventory!!
        val changedAmount = 10
        val changedResource = ResourceType.COAL
        val event = generateRobotResourceRemovedIntegrationEvent(inventoryBeforeChange, changedAmount, changedResource)

        internalEventListener.listenForInternalEvents(event)

        val expected = resourceInventoryToInventoryMapper.mapResourceInventoryToInventory(event.resourceInventory)
        validateInventory(expected)
    }

    @Test
    fun test_robotResourceRemovedIntegrationEvent_iron() {
        spawnRobot()
        val inventoryBeforeChange = robotRepository.getRobotById(robotId)?.inventory!!
        val changedAmount = 10
        val changedResource = ResourceType.IRON
        val event = generateRobotResourceRemovedIntegrationEvent(inventoryBeforeChange, changedAmount, changedResource)

        internalEventListener.listenForInternalEvents(event)

        val expected = resourceInventoryToInventoryMapper.mapResourceInventoryToInventory(event.resourceInventory)
        validateInventory(expected)
    }

    @Test
    fun test_robotResourceRemovedIntegrationEvent_gem() {
        spawnRobot()
        val inventoryBeforeChange = robotRepository.getRobotById(robotId)?.inventory!!
        val changedAmount = 10
        val changedResource = ResourceType.GEM
        val event = generateRobotResourceRemovedIntegrationEvent(inventoryBeforeChange, changedAmount, changedResource)

        internalEventListener.listenForInternalEvents(event)

        val expected = resourceInventoryToInventoryMapper.mapResourceInventoryToInventory(event.resourceInventory)
        validateInventory(expected)
    }

    @Test
    fun test_robotResourceRemovedIntegrationEvent_gold() {
        spawnRobot()
        val inventoryBeforeChange = robotRepository.getRobotById(robotId)?.inventory!!
        val changedAmount = 10
        val changedResource = ResourceType.GOLD
        val event = generateRobotResourceRemovedIntegrationEvent(inventoryBeforeChange, changedAmount, changedResource)

        internalEventListener.listenForInternalEvents(event)

        val expected = resourceInventoryToInventoryMapper.mapResourceInventoryToInventory(event.resourceInventory)
        validateInventory(expected)
    }

    @Test
    fun test_robotResourceRemovedIntegrationEvent_platin() {
        spawnRobot()
        val inventoryBeforeChange = robotRepository.getRobotById(robotId)?.inventory!!
        val changedAmount = -10
        val changedResource = ResourceType.PLATIN
        val event = generateRobotResourceRemovedIntegrationEvent(inventoryBeforeChange, changedAmount, changedResource)

        internalEventListener.listenForInternalEvents(event)

        val expected = resourceInventoryToInventoryMapper.mapResourceInventoryToInventory(event.resourceInventory)
        validateInventory(expected)
    }

    @Test
    fun test_robotResourceMinedIntegrationEvent_coal() {
        spawnRobot()
        val (inventoryBeforeChange, minedResourcesBeforeChange) = getInventoryAndMinedResourcesForRobot(robotId)
        val amount = 10
        val resourceType = ResourceType.COAL
        val event = generateRobotResourceMinedIntegrationEvent(inventoryBeforeChange, amount, resourceType)
        val expectedMinedResources = generateMinedResourcesAfterChange(minedResourcesBeforeChange, resourceType, amount)
        val expectedInventory = resourceInventoryToInventoryMapper.mapResourceInventoryToInventory(event.resourceInventory)

        internalEventListener.listenForInternalEvents(event)

        validateInventoryAndMinedResources(expectedInventory, expectedMinedResources)
    }

    @Test
    fun test_robotResourceMinedIntegrationEvent_iron() {
        spawnRobot()
        val (inventoryBeforeChange, minedResourcesBeforeChange) = getInventoryAndMinedResourcesForRobot(robotId)
        val amount = 10
        val resourceType = ResourceType.IRON
        val event = generateRobotResourceMinedIntegrationEvent(inventoryBeforeChange, amount, resourceType)
        val expectedMinedResources = generateMinedResourcesAfterChange(minedResourcesBeforeChange, resourceType, amount)
        val expectedInventory = resourceInventoryToInventoryMapper.mapResourceInventoryToInventory(event.resourceInventory)

        internalEventListener.listenForInternalEvents(event)

        validateInventoryAndMinedResources(expectedInventory, expectedMinedResources)
    }

    @Test
    fun test_robotResourceMinedIntegrationEvent_gem() {
        spawnRobot()
        val (inventoryBeforeChange, minedResourcesBeforeChange) = getInventoryAndMinedResourcesForRobot(robotId)
        val amount = 10
        val resourceType = ResourceType.GEM
        val event = generateRobotResourceMinedIntegrationEvent(inventoryBeforeChange, amount, resourceType)
        val expectedMinedResources = generateMinedResourcesAfterChange(minedResourcesBeforeChange, resourceType, amount)
        val expectedInventory = resourceInventoryToInventoryMapper.mapResourceInventoryToInventory(event.resourceInventory)

        internalEventListener.listenForInternalEvents(event)

        validateInventoryAndMinedResources(expectedInventory, expectedMinedResources)
    }

    @Test
    fun test_robotResourceMinedIntegrationEvent_gold() {
        spawnRobot()
        val (inventoryBeforeChange, minedResourcesBeforeChange) = getInventoryAndMinedResourcesForRobot(robotId)
        val amount = 10
        val resourceType = ResourceType.GOLD
        val event = generateRobotResourceMinedIntegrationEvent(inventoryBeforeChange, amount, resourceType)
        val expectedMinedResources = generateMinedResourcesAfterChange(minedResourcesBeforeChange, resourceType, amount)
        val expectedInventory = resourceInventoryToInventoryMapper.mapResourceInventoryToInventory(event.resourceInventory)

        internalEventListener.listenForInternalEvents(event)

        validateInventoryAndMinedResources(expectedInventory, expectedMinedResources)
    }

    @Test
    fun test_robotResourceMinedIntegrationEvent_platin() {
        spawnRobot()
        val (inventoryBeforeChange, minedResourcesBeforeChange) = getInventoryAndMinedResourcesForRobot(robotId)
        val amount = 10
        val resourceType = ResourceType.PLATIN
        val event = generateRobotResourceMinedIntegrationEvent(inventoryBeforeChange, amount, resourceType)
        val expectedMinedResources = generateMinedResourcesAfterChange(minedResourcesBeforeChange, resourceType, amount)
        val expectedInventory = resourceInventoryToInventoryMapper.mapResourceInventoryToInventory(event.resourceInventory)

        internalEventListener.listenForInternalEvents(event)

        validateInventoryAndMinedResources(expectedInventory, expectedMinedResources)
    }

    @Test
    fun test_robotRegeneratedIntegrationEvent() {
        spawnRobot()
        val changeAmount = 10
        val availableEnergy = robotRepository.getRobotById(robotId)?.energy!! + changeAmount
        val event = RobotRegeneratedEvent(robotId, availableEnergy, Instant.now())

        internalEventListener.listenForInternalEvents(event)

        val robot = robotRepository.getRobotById(robotId)!!
        with(robot) {
            assertThat(energy).isEqualTo(availableEnergy)
        }
    }

    @Test
    fun test_robotMovedIntegrationEvent() {
        spawnRobot()
        val originPlanetId = UUID.randomUUID()
        val destinationPlanetId = UUID.randomUUID()
        val movementDifficulty = 1
        val remainingEnergy = 10
        val event = RobotMovedEvent(
            robotId,
            remainingEnergy, MovementPlanetProperties(originPlanetId, movementDifficulty), MovementPlanetProperties(
                destinationPlanetId,
                movementDifficulty
            ), Instant.now()
        )

        internalEventListener.listenForInternalEvents(event)

        val robot = robotRepository.getRobotById(robotId)!!
        with(robot) {
            assertThat(energy).isEqualTo(remainingEnergy)
            assertThat(currentPlanetId).isEqualTo(destinationPlanetId)
            assertThat(previousPlanetId).isEqualTo(originPlanetId)
        }
    }

    @Test
    fun test_robotAttackedIntegrationEvent_nonlethalFight() {
        val attackerId = UUID.randomUUID()
        val defenderId = UUID.randomUUID()
        val attackerProperties = generateFightingPropertiesForRobot(attackerId, 20, 0)
        val defenderProperties = generateFightingPropertiesForRobot(defenderId, 0, 20)
        val event = RobotAttackedEvent(attackerProperties, defenderProperties, Instant.now())

        internalEventListener.listenForInternalEvents(event)

        checkNonlethalFightResultForRobot(attackerProperties, attackerId, defenderId, true)
        checkNonlethalFightResultForRobot(defenderProperties, attackerId, defenderId, false)
    }

    @Test
    fun test_robotAttackedIntegrationEvent_kill() {
        val attackerId = UUID.randomUUID()
        val defenderId = UUID.randomUUID()
        val attackerProperties = generateFightingPropertiesForRobot(attackerId, 20, 0)
        val defenderProperties = generateFightingPropertiesForRobot(defenderId, 0, health)
        val event = RobotAttackedEvent(attackerProperties, defenderProperties, Instant.now())

        internalEventListener.listenForInternalEvents(event)

        val defender = robotRepository.getRobotById(defenderId)!!
        with(defender) {
            assertThat(alive).isFalse
            assertThat(destroyedBy).isEqualTo(attackerId)
        }
    }

    private fun checkNonlethalFightResultForRobot(
        fightingRobotProperties: FightingRobotProperties,
        attackerId: UUID,
        defenderId: UUID,
        isAttacker: Boolean
    ) {
        val id = if (isAttacker) attackerId else defenderId
        val robot = robotRepository.getRobotById(id)!!
        with(robot) {
            assertThat(energy).isEqualTo(fightingRobotProperties.energy)
            assertThat(health).isEqualTo(fightingRobotProperties.health)
            val fights = if (isAttacker) offensiveFights else defensiveFights
            assertThat(fights.firstOrNull()!!.attackingRobotId).isEqualTo(attackerId)
            assertThat(fights.firstOrNull()!!.defendingRobotId).isEqualTo(defenderId)
            assertThat(alive).isTrue
            assertThat(destroyedBy).isNull()
        }
    }

    private fun generateFightingPropertiesForRobot(
        robotId: UUID,
        energyDraw: Int,
        damage: Int
    ): FightingRobotProperties {
        spawnRobot(robotId)
        return robotRepository.getRobotById(robotId)!!.let {
            val alive = it.health - damage > 0
            FightingRobotProperties(robotId, it.health - damage, it.energy - energyDraw, alive)
        }
    }

    private fun getInventoryAndMinedResourcesForRobot(robotId: UUID): Pair<Inventory, MinedResources> {
        val robot = robotRepository.getRobotById(robotId)!!
        val inventoryBeforeChange = robot.inventory
        val minedResourcesBeforeChange = robot.minedResources
        return Pair(inventoryBeforeChange, minedResourcesBeforeChange)
    }

    private fun generateMinedResourcesAfterChange(
        minedResourcesBeforeChange: MinedResources,
        minedResource: ResourceType,
        minedAmount: Int
    ) : MinedResources {
        return minedResourcesBeforeChange.let {
            MinedResources(
                if (minedResource == ResourceType.COAL) it.coal + minedAmount else it.coal,
                if (minedResource == ResourceType.IRON) it.iron + minedAmount else it.iron,
                if (minedResource == ResourceType.GEM) it.gem + minedAmount else it.gem,
                if (minedResource == ResourceType.GOLD) it.gold + minedAmount else it.gold,
                if (minedResource == ResourceType.PLATIN) it.platin + minedAmount else it.platin,
            )
        }
    }

    private fun generateRobotResourceMinedIntegrationEvent(
        inventoryBeforeChange: Inventory,
        minedAmount: Int,
        minedResource: ResourceType
    ) : RobotResourceMinedEvent {
        val resourceInventory = inventoryBeforeChange.let {
            ResourceInventory(
                if (minedResource == ResourceType.COAL) it.coal + minedAmount else it.coal,
                if (minedResource == ResourceType.IRON) it.iron + minedAmount else it.iron,
                if (minedResource == ResourceType.GEM) it.gem + minedAmount else it.gem,
                if (minedResource == ResourceType.GOLD) it.gold + minedAmount else it.gold,
                if (minedResource == ResourceType.PLATIN) it.platin + minedAmount else it.platin,
            )
        }
        return RobotResourceMinedEvent(robotId, minedAmount, minedResource, resourceInventory, Instant.now())
    }

    fun generateRobotUpgradedIntegrationEvent(oldRobotProperties: RobotProperties, level: Int, upgrade: UpgradeType) : RobotUpgradedEvent {
        val robotProperties = generateUpgradedRobotProperties(oldRobotProperties, level, upgrade)
        return RobotUpgradedEvent(robotId, level, upgrade, robotProperties)
    }

    fun generateUpgradedRobotProperties(robotProperties: RobotProperties, level: Int, upgrade: UpgradeType) : RobotProperties {
        return with(robotProperties) {
            val storageLevel = if (upgrade == UpgradeType.STORAGE) level else storageLevel
            val healthLevel = if (upgrade == UpgradeType.HEALTH) level else healthLevel
            val damageLevel = if (upgrade == UpgradeType.DAMAGE) level else damageLevel
            val miningSpeedLevel = if (upgrade == UpgradeType.MINING_SPEED) level else miningSpeedLevel
            val miningLevel = if (upgrade == UpgradeType.MINING) level else miningLevel
            val energyLevel = if (upgrade == UpgradeType.MAXIMUM_ENERGY) level else energyLevel
            val energyRegenerationLevel = if (upgrade == UpgradeType.ENERGY_REGENERATION) level else energyRegenerationLevel
            RobotProperties(
                robotId,
                alive,
                playerId,
                maxHealth,
                maxEnergy,
                energyRegeneration,
                attackDamage,
                miningSpeed,
                health,
                energy,
                healthLevel,
                damageLevel,
                miningSpeedLevel,
                miningLevel,
                energyLevel,
                energyRegenerationLevel,
                planet,
                with(inventory) { Inventory(resources, storageLevel, usedStorage, maxStorage, full) }
            )
        }
    }

    private fun validateInventoryAndMinedResources(
        expectedInventory: Inventory,
        expectedMinedResources: MinedResources
    ) {
        val player = playerRepository.getPlayerById(playerId)
        assertThat(player).isNotNull
        player!!.robots.firstOrNull()!!.let {
            assertThat(it.inventory).usingRecursiveComparison().isEqualTo(expectedInventory)
            assertThat(it.minedResources).usingRecursiveComparison().isEqualTo(expectedMinedResources)
        }
    }

    private fun generateRobotResourceRemovedIntegrationEvent(
        inventoryBeforeChange: Inventory,
        changedAmount: Int,
        changedResource: ResourceType
    ): RobotResourceRemovedEvent {
        val resourceInventory = generateResourceInventoryWithChange(
            inventoryBeforeChange,
            changedResource,
            changedAmount
        )
        val timestamp = Instant.now()
        val event =
            RobotResourceRemovedEvent(robotId, changedAmount, changedResource, resourceInventory, timestamp)
        return event
    }

    private fun generateResourceInventoryWithChange(
        inventoryBeforeChange: Inventory,
        changedResource: ResourceType,
        changedAmount: Int
    ): ResourceInventory {
        inventoryBeforeChange.let {
            var coal = it.coal
            var iron = it.iron
            var gem = it.gem
            var gold = it.gold
            var platin = it.platin
            when (changedResource) {
                ResourceType.COAL -> coal -= changedAmount
                ResourceType.IRON -> iron -= changedAmount
                ResourceType.GEM -> gem -= changedAmount
                ResourceType.GOLD -> gold -= changedAmount
                ResourceType.PLATIN -> platin -= changedAmount
            }
            return ResourceInventory(coal, iron, gem, gold, platin)
        }
    }

    private fun validateInventory(
        expected: Inventory
    ) {
        val player = playerRepository.getPlayerById(playerId)
        assertThat(player).isNotNull
        with(player!!) {
            val robot = robots.firstOrNull()
            assertThat(robot?.inventory).usingRecursiveComparison().isEqualTo(expected)
        }
    }

    private fun validateHealthAndEnergy(availableEnergy: Int, availableHealth: Int ) {
        val player = playerRepository.getPlayerById(playerId)
        assertThat(player).isNotNull
        player?.run {
            val robot = robots.firstOrNull()
            assertThat(robot?.energy).isEqualTo(availableEnergy)
            assertThat(robot?.health).isEqualTo(availableHealth)
        }
    }

    private fun validateLevelUpgrade(upgrade: UpgradeType, level: Int) {
        val player = playerRepository.getPlayerById(playerId)
        assertThat((player)).isNotNull
        player?.run {
            val robot = robots.firstOrNull()
            val result = when(upgrade) {
                UpgradeType.STORAGE -> robot?.storageLevel
                UpgradeType.HEALTH -> robot?.healthLevel
                UpgradeType.DAMAGE -> robot?.damageLevel
                UpgradeType.MINING_SPEED -> robot?.miningSpeedLevel
                UpgradeType.MINING -> robot?.miningLevel
                UpgradeType.MAXIMUM_ENERGY -> robot?.energyLevel
                UpgradeType.ENERGY_REGENERATION -> robot?.energyRegenerationLevel
            }
            assertThat(result).isEqualTo(level)
        }
    }

    private fun spawnRobot(robotId: UUID = this.robotId): RobotProperties {
        val robotProperties = generateRobotProperties(robotId)
        val event = RobotSpawnedEvent(
            robotProperties
        )
        internalEventListener.listenForInternalEvents(event)
        return robotProperties
    }

    private fun generateRobotProperties(robotId: UUID) = RobotProperties(
        robotId,
        alive,
        playerId,
        maxHealth,
        maxEnergy,
        energyRegeneration,
        attackDamage,
        miningSpeed,
        health,
        energy,
        healthLevel,
        damageLevel,
        miningSpeedLevel,
        miningLevel,
        energyLevel,
        energyRegenerationLevel,
        spawnPlanet,
        spawnInventory
    )

}
