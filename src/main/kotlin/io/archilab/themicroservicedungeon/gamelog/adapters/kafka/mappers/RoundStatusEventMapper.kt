package io.archilab.themicroservicedungeon.gamelog.adapters.kafka.mappers

import io.archilab.themicroservicedungeon.gamelog.domain.events.InternalEvent
import io.archilab.themicroservicedungeon.gamelog.domain.events.game.RoundStatusEvent
import io.archilab.themicroservicedungeon.gamelog.domain.exceptions.MapperUnknownEventTypeException
import org.apache.kafka.clients.consumer.ConsumerRecord
import org.springframework.stereotype.Component
import java.time.Instant

@Component
class RoundStatusEventMapper : KafkaToInternalEventMapper {

    private val objectMapper = InjectingObjectMapper()

    override fun mapConsumerRecordToEvent(
        type: String,
        timestamp: String,
        payload: ConsumerRecord<String, ByteArray>
    ): InternalEvent {
        if (type != "RoundStatus") throw MapperUnknownEventTypeException()
        return objectMapper.mapInjectingValues(mapOf(Pair("timestamp", Instant.parse(timestamp))), payload.value(), RoundStatusEvent::class.java)
    }

}
