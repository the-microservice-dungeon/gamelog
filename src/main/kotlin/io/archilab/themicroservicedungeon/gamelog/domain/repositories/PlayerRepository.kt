package io.archilab.themicroservicedungeon.gamelog.domain.repositories

import io.archilab.themicroservicedungeon.gamelog.domain.entities.Player
import java.util.UUID

interface PlayerRepository {

    fun savePlayer (player: Player)

    fun getPlayerById (playerId: UUID) : Player?

    fun getPlayers(): List<Player>

}