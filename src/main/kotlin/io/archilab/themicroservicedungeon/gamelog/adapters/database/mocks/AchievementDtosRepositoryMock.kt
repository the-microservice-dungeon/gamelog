package io.archilab.themicroservicedungeon.gamelog.adapters.database.mocks

import io.archilab.themicroservicedungeon.gamelog.domain.achievements.AchievementsManager
import io.archilab.themicroservicedungeon.gamelog.domain.dtos.*
import io.archilab.themicroservicedungeon.gamelog.domain.enums.AchievementType
import io.archilab.themicroservicedungeon.gamelog.domain.repositories.AchievementDtosRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Profile
import org.springframework.stereotype.Repository
import java.util.*
import kotlin.random.Random

@Repository
@Profile("api-test")
class AchievementDtosRepositoryMock: AchievementDtosRepository {

    private val playerNames = listOf(
        "foo",
        "bar",
        "baz",
        "lorem",
        "ipsum"
    )

    @Autowired
    private lateinit var achievementsManager: AchievementsManager

    private val gameId = UUID.randomUUID()
    override fun getAchievements(): AchievementsDto {
        val playerAchievements = generateRandomPlayerAchievements()
        return AchievementsDto(gameId, playerAchievements)
    }

    private fun generateRandomPlayerAchievements() : List<PlayerAchievementDto> {
        return playerNames.flatMap { name ->
            val player = PlayerDto(UUID.randomUUID(), name)
             (0 until Random.nextInt(1, 5)).mapNotNull {
                generateRandomAchievement()?.let {
                    PlayerAchievementDto(
                        player,
                        gameId,
                        it
                    )
                }
            }
        }
    }

    private fun generateRandomAchievement() : AchievementDto? {
        val achievementType = AchievementType.values().random()
        return achievementsManager.getAchievement(achievementType)?.let {
            AchievementDto(it.name, it.imageUrl, achievementsManager.getAchievementCategory(achievementType))
        }
    }

}