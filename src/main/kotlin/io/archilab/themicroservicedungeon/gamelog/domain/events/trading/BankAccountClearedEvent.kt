package io.archilab.themicroservicedungeon.gamelog.domain.events.trading

import com.fasterxml.jackson.annotation.JacksonInject
import io.archilab.themicroservicedungeon.gamelog.domain.events.InternalEvent
import java.time.Instant
import java.util.*

data class BankAccountClearedEvent(
    val playerId: UUID,
    val balance: Int,
    @JacksonInject("timestamp")
    val timestamp: Instant
) : InternalEvent