package io.archilab.themicroservicedungeon.gamelog.domain.dtos

import java.util.*

data class PlayerScoreboardTrophiesDto (
    val player: PlayerDto,
    val scoreboardTrophies: List<ScoreboardTrophyDto>,
    val gameID: UUID
)