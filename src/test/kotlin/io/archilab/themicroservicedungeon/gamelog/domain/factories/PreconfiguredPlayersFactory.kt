package io.archilab.themicroservicedungeon.gamelog.domain.factories

import io.archilab.themicroservicedungeon.gamelog.domain.entities.Player
import io.archilab.themicroservicedungeon.gamelog.domain.entities.Robot
import io.archilab.themicroservicedungeon.gamelog.domain.enums.*
import java.math.BigDecimal
import java.util.*

class PreconfiguredPlayersFactory(
    private val lowValue: Int = 1,
    private val mediumValue: Int = 5,
    private val highValue: Int = 10
) {

    private val robotLevels = 1
    private val robotCapacities = 10

    fun createFocusedPlayer(focus: ScoreboardCategory, playerSuccess: PlayerSuccess): Player {
        val player = Player(UUID.randomUUID(), "player_${focus.name}_${playerSuccess.name}")
        configurePlayerForFocus(player, focus, playerSuccess)
        return player
    }

    fun createAchievingPlayerPair(achievementType: AchievementType): Pair<Player, Player> {
        val notAchieving = Player(UUID.randomUUID(), "not_achieving_" + achievementType.name)
        val achieving = Player(UUID.randomUUID(), "achieving" + achievementType.name)
        when (achievementType) {
            AchievementType.FIGHTING_BRONZE -> configurePlayerPairForFightingAchievement(notAchieving, achieving, AchievementType.FIGHTING_BRONZE)
            AchievementType.FIGHTING_SILVER -> configurePlayerPairForFightingAchievement(notAchieving, achieving, AchievementType.FIGHTING_SILVER)
            AchievementType.FIGHTING_GOLD -> configurePlayerPairForFightingAchievement(notAchieving, achieving, AchievementType.FIGHTING_GOLD)
            AchievementType.MINING_BRONZE -> configurePlayerPairForMiningAchievement(notAchieving, achieving, AchievementType.MINING_BRONZE)
            AchievementType.MINING_SILVER -> configurePlayerPairForMiningAchievement(notAchieving, achieving, AchievementType.MINING_SILVER)
            AchievementType.MINING_GOLD -> configurePlayerPairForMiningAchievement(notAchieving, achieving, AchievementType.MINING_GOLD)
            AchievementType.TRADING_BRONZE -> configurePlayerPairForTradingAchievement(notAchieving, achieving, AchievementType.TRADING_BRONZE)
            AchievementType.TRADING_SILVER -> configurePlayerPairForTradingAchievement(notAchieving, achieving, AchievementType.TRADING_SILVER)
            AchievementType.TRADING_GOLD -> configurePlayerPairForTradingAchievement(notAchieving, achieving, AchievementType.TRADING_GOLD)
            AchievementType.TRAVELING_BRONZE -> configurePlayerPairForTravelingAchievement(notAchieving, achieving, AchievementType.TRAVELING_BRONZE)
            AchievementType.TRAVELING_SILVER -> configurePlayerPairForTravelingAchievement(notAchieving, achieving, AchievementType.TRAVELING_SILVER)
            AchievementType.TRAVELING_GOLD -> configurePlayerPairForTravelingAchievement(notAchieving, achieving, AchievementType.TRAVELING_GOLD)
        }
        return Pair(notAchieving, achieving)
    }

    private fun configurePlayerPairForFightingAchievement(
        notAchieving: Player,
        achieving: Player,
        achievementType: AchievementType
    ) {
        addFightsForAchievement(notAchieving, achievementType, false)
        addFightsForAchievement(achieving, achievementType, true)
    }

    private fun configurePlayerPairForMiningAchievement(
        notAchieving: Player,
        achieving: Player,
        achievementType: AchievementType
    ) {
        addResourcesForAchievement(notAchieving, achievementType, false)
        addResourcesForAchievement(achieving, achievementType, true)
    }

    private fun configurePlayerPairForTradingAchievement(
        notAchieving: Player,
        achieving: Player,
        achievementType: AchievementType
    ) {
        addTransactionsForAchievement(notAchieving, achievementType, false)
        addTransactionsForAchievement(achieving, achievementType, true)
    }

    private fun configurePlayerPairForTravelingAchievement(
        notAchieving: Player,
        achieving: Player,
        achievementType: AchievementType
    ) {
        addMovementsForAchievement(notAchieving, achievementType, false)
        addMovementsForAchievement(achieving, achievementType, true)
    }

    private fun addFightsForAchievement(player: Player, achievementType: AchievementType, earningAchievement: Boolean) {
        val fightCount = when (achievementType) {
            AchievementType.FIGHTING_BRONZE -> 1
            AchievementType.FIGHTING_SILVER -> 10
            AchievementType.FIGHTING_GOLD -> 25
            else -> 0
        }
        player.addRobot(createStandardizedRobot(player.id).apply {
            repeat(fightCount) {
                addFightToRobot(earningAchievement || it < fightCount - 1)
            }
        })
    }

    private fun addResourcesForAchievement(player: Player, achievementType: AchievementType, earningAchievement: Boolean) {
        val resourcesCount = when(achievementType) {
            AchievementType.MINING_BRONZE -> 1
            AchievementType.MINING_SILVER -> 25
            AchievementType.MINING_GOLD -> 150
            else -> 0
        }
        player.addRobot(createStandardizedRobot(player.id).apply {
            minedResources.coal = if (earningAchievement) resourcesCount else resourcesCount - 1
        })
    }

    private fun addTransactionsForAchievement(player: Player, achievementType: AchievementType, earningAchievement: Boolean) {
        val transactionsAmount = when(achievementType) {
            AchievementType.TRADING_BRONZE -> 1
            AchievementType.TRADING_SILVER -> 1000
            AchievementType.TRADING_GOLD -> 10000
            else -> 0
        }
        player.addRobot(createStandardizedRobot(player.id).also {
            val totalAmount = if(earningAchievement) transactionsAmount else transactionsAmount - 1
            player.bookSellingTransaction(it.robotId, TradableType.RESOURCE, "coal", totalAmount, BigDecimal(1), BigDecimal(totalAmount)
            )
        })
    }

    private fun addMovementsForAchievement(player: Player, achievementType: AchievementType, earningAchievement: Boolean) {
        val planetsCount = when(achievementType) {
            AchievementType.TRAVELING_BRONZE -> 1
            AchievementType.TRAVELING_SILVER -> 15
            AchievementType.TRAVELING_GOLD -> 40
            else -> 0
        }
        player.addRobot(createStandardizedRobot(player.id).also {robot ->
            val movements = if (earningAchievement) planetsCount else planetsCount - 1
            repeat(movements) {
                robot.addMovement(UUID.randomUUID(), UUID.randomUUID(), 1)
            }
        })
    }

    private fun configurePlayerForFocus(player: Player, focus: ScoreboardCategory, playerSuccess: PlayerSuccess) {
        when (focus) {
            ScoreboardCategory.FIGHTING -> player.configureFightingFocus(playerSuccess)
            ScoreboardCategory.MINING -> player.configureMiningFocus(playerSuccess)
            ScoreboardCategory.TRADING -> player.configureTradingFocus(playerSuccess)
            ScoreboardCategory.TRAVELING -> player.configureTravelingFocus(playerSuccess)
            ScoreboardCategory.GAME -> player.apply {
                configureFightingFocus(playerSuccess)
                configureMiningFocus(playerSuccess)
                configureTradingFocus(playerSuccess)
                configureTravelingFocus(playerSuccess)
            }
        }
    }

    private fun Player.configureMiningFocus(playerSuccess: PlayerSuccess) {
        val robot = createStandardizedRobot(id)
        robot.minedResources.apply {
            when (playerSuccess) {
                PlayerSuccess.LOW -> {
                    coal = lowValue
                    iron = lowValue
                    gem = lowValue
                    gold = lowValue
                    platin = lowValue
                }

                PlayerSuccess.MEDIUM -> {
                    coal = mediumValue
                    iron = mediumValue
                    gem = mediumValue
                    gold = mediumValue
                    platin = mediumValue
                }

                PlayerSuccess.HIGH -> {
                    coal = highValue
                    iron = highValue
                    gem = highValue
                    gold = highValue
                    platin = highValue
                }
            }
        }
        addRobot(robot)
    }

    private fun Player.configureFightingFocus(playerSuccess: PlayerSuccess) {
        val robot = createStandardizedRobot(id)
        robot.apply {
            val range = when (playerSuccess) {
                PlayerSuccess.LOW -> 0 until lowValue
                PlayerSuccess.MEDIUM -> 0 until mediumValue
                PlayerSuccess.HIGH -> 0 until highValue
            }
            for (i in range) {
                val defenderKilled = i % 2 == 0
                addFightToRobot(defenderKilled)
            }
        }
        addRobot(robot)
    }

    private fun Robot.addFightToRobot(defenderKilled: Boolean) {
        addFight(robotId, UUID.randomUUID(), highValue, highValue, true, !defenderKilled)
    }

    private fun Player.configureTradingFocus(playerSuccess: PlayerSuccess) {
        val robot = createStandardizedRobot(id)
        addRobot(robot)
        val pricePerUnit = when (playerSuccess) {
            PlayerSuccess.LOW -> lowValue
            PlayerSuccess.MEDIUM -> mediumValue
            PlayerSuccess.HIGH -> highValue
        }
        for (i in 0 until 10) {
            val amount = 10
            val total = amount * pricePerUnit
            bookSellingTransaction(
                robot.robotId,
                TradableType.RESOURCE,
                "coal",
                amount,
                pricePerUnit.toBigDecimal(),
                total.toBigDecimal()
            )
        }
    }

    private fun Player.configureTravelingFocus(playerSuccess: PlayerSuccess) {
        val robot = createStandardizedRobot(id)
        val movementsCount = when (playerSuccess) {
            PlayerSuccess.LOW -> lowValue
            PlayerSuccess.MEDIUM -> mediumValue
            PlayerSuccess.HIGH -> highValue
        }
        robot.apply {
            for (i in 0 until movementsCount) {
                addMovement(UUID.randomUUID(), UUID.randomUUID())
            }
        }
        addRobot(robot)
    }

    private fun createStandardizedRobot(playerId: UUID): Robot {
        return Robot(
            UUID.randomUUID(),
            true,
            playerId,
            robotCapacities,
            robotCapacities,
            robotLevels,
            robotLevels,
            robotLevels,
            robotCapacities,
            robotCapacities,
            robotLevels,
            robotLevels,
            robotLevels,
            robotLevels,
            robotLevels,
            robotLevels,
            robotLevels
        )
    }

}