package io.archilab.themicroservicedungeon.gamelog.domain.repositories

import io.archilab.themicroservicedungeon.gamelog.domain.entities.Game
import java.time.Instant
import java.util.*

interface GameRepository {

    fun getGameById(gameId: UUID): Game?

    fun saveGame(game: Game)

    fun getGameRunningAtTimestamp(timestamp: Instant): Game?

    fun getNewestUnfinishedGame(): Game?

    fun getNewestGame(): Game?

    fun getUnfinishedGames(): List<Game>

    fun getLastFinishedGame(): Game?

}