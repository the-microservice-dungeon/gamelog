package io.archilab.themicroservicedungeon.gamelog.domain.events.game

import io.archilab.themicroservicedungeon.gamelog.domain.events.InternalEvent
import java.util.*

data class PlayerStatusEvent(
    val gameId: UUID,
    val playerId: UUID,
    val name: String
) : InternalEvent