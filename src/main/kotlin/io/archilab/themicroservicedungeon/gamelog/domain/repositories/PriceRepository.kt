package io.archilab.themicroservicedungeon.gamelog.domain.repositories

import io.archilab.themicroservicedungeon.gamelog.domain.entities.PricesList

interface PriceRepository {

    fun getNewestPrices() : PricesList?

    fun savePricesList(pricesList: PricesList)

}