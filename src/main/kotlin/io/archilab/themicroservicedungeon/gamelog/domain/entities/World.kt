package io.archilab.themicroservicedungeon.gamelog.domain.entities

import io.archilab.themicroservicedungeon.gamelog.domain.enums.WorldStatus
import org.hibernate.annotations.Type
import java.util.*
import javax.persistence.*

@Entity
class World() {

    @Id
    @Column
    @Type(type = "uuid-char")
    lateinit var id: UUID

    @Column
    lateinit var status: WorldStatus

    @OneToMany(cascade = [CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH])
    lateinit var planets: Set<Planet>

    constructor(id: UUID, status: WorldStatus, planets: Set<Planet>) :this() {
        this.id = id
        this.status = status
        this.planets = planets
    }

}