package io.archilab.themicroservicedungeon.gamelog.domain.eventhandlers

import RobotSpawnedEvent
import io.archilab.themicroservicedungeon.gamelog.domain.entities.Player
import io.archilab.themicroservicedungeon.gamelog.domain.entities.Robot
import io.archilab.themicroservicedungeon.gamelog.domain.events.InternalEvent
import io.archilab.themicroservicedungeon.gamelog.domain.events.robot.*
import io.archilab.themicroservicedungeon.gamelog.domain.mappers.ResourceInventoryToInventoryMapper
import io.archilab.themicroservicedungeon.gamelog.domain.mappers.SpawnRobotPropertiesToRobotMapper
import io.archilab.themicroservicedungeon.gamelog.domain.repositories.PlayerRepository
import io.archilab.themicroservicedungeon.gamelog.domain.repositories.RobotRepository
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component
import org.springframework.transaction.annotation.Transactional
import java.util.*

@Component
class RobotIntegrationEventHandler {

    @Autowired
    private lateinit var playerRepository: PlayerRepository

    @Autowired
    private lateinit var robotRepository: RobotRepository

    private val robotPropertiesToRobotMapper = SpawnRobotPropertiesToRobotMapper()

    private val resourceInventoryToInventoryMapper = ResourceInventoryToInventoryMapper()

    private val logger = LoggerFactory.getLogger(RobotIntegrationEventHandler::class.java)

    @Transactional
    fun handleRobotSpawnedEvent(event: RobotSpawnedEvent) {
        val player =
            playerRepository.getPlayerById(event.robot.playerId) ?: Player(event.robot.playerId).also {
                logger.warn("Player ${event.robot.playerId} doesn't exist and is now auto-initialized")
            }
        player.apply {
            val robot = robotPropertiesToRobotMapper.mapRobotPropertiesToRobot(event.robot)
            addRobot(robot)
            playerRepository.savePlayer(this)
        }
    }

    @Transactional
    fun handleRobotUpgradedEvent(event: RobotUpgradedEvent) {
        val robot = getRobotWithErrorLogging(event.robotId, event)
        robot?.apply {
            installUpgrade(event.level, event.upgrade)
            robotRepository.save(this)
        }
    }

    @Transactional
    fun handleRobotRestoredAttributesEvent(event: RobotRestoredAttributesEvent) {
        val robot = getRobotWithErrorLogging(event.robotId, event)
        robot?.apply {
            energy = event.availableEnergy
            health = event.availableHealth
            robotRepository.save(this)
        }
    }

    @Transactional
    fun handleRobotResourceRemovedEvent(event: RobotResourceRemovedEvent) {
        val robot = getRobotWithErrorLogging(event.robotId, event)
        robot?.apply {
            inventory = resourceInventoryToInventoryMapper.mapResourceInventoryToInventory(event.resourceInventory)
            robotRepository.save(this)
        }
    }

    @Transactional
    fun handleRobotResourceMinedEvent(event: RobotResourceMinedEvent) {
        val robot = getRobotWithErrorLogging(event.robotId, event)
        robot?.apply {
            event.let {
                val inventoryAfterMining = resourceInventoryToInventoryMapper.mapResourceInventoryToInventory(it.resourceInventory)
                processMinedResources(it.minedResource, it.minedAmount, inventoryAfterMining)
            }
            robotRepository.save(this)
        }
    }

    @Transactional
    fun handleRobotRegeneratedEvent(event: RobotRegeneratedEvent) {
        val robot = getRobotWithErrorLogging(event.robotId, event)
        robot?.apply {
            energy = event.availableEnergy
            robotRepository.save(this)
        }
    }

    @Transactional
    fun handleRobotMovedEvent(event: RobotMovedEvent) {
        val robot = getRobotWithErrorLogging(event.robotId, event)
        robot?.apply {
            addMovement(event.originPlanet.planetId, event.destinationPlanet.planetId, event.remainingEnergy)
            robotRepository.save(this)
        }
    }

    @Transactional
    fun handleRobotAttackedEvent(event: RobotAttackedEvent) {
        event.let {
            val attackerId = it.attacker.id
            val defenderId = it.defender.id
            val attacker = getRobotWithErrorLogging(attackerId, event)
            val defender = getRobotWithErrorLogging(defenderId, event)
            it.attacker.run {
                applyFightToRobot(attacker, attackerId, defenderId, energy, health, alive, it.defender.alive)
            }
            it.defender.run {
                applyFightToRobot(defender, attackerId, defenderId, energy, health, alive, alive)
            }
        }
    }

    private fun applyFightToRobot(
        robot: Robot?,
        attackerId: UUID,
        defenderId: UUID,
        energy: Int,
        health: Int,
        alive: Boolean,
        defenderAlive: Boolean
    ) {
        robot?.apply {
            addFight(attackerId, defenderId, energy, health, alive, defenderAlive)
            robotRepository.save(this)
        }
    }

    private fun getRobotWithErrorLogging(robotId: UUID, eventThatRequiresRobot: InternalEvent): Robot? {
        val robot = robotRepository.getRobotById(robotId) ?: null.also {
            logRobotNotFoundErrorForEvent(robotId, eventThatRequiresRobot)
        }
        return robot
    }

    private fun logRobotNotFoundErrorForEvent(robotId: UUID, event: InternalEvent) {
        logger.error("Robot $robotId doesn't exist! Can't persist ${event::class.simpleName}")
    }

}
