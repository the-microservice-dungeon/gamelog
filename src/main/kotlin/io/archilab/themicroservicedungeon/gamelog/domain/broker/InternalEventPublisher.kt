package io.archilab.themicroservicedungeon.gamelog.domain.broker

import io.archilab.themicroservicedungeon.gamelog.domain.events.InternalEvent
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.ApplicationEventPublisher
import org.springframework.stereotype.Component

@Component
class InternalEventPublisher {

    @Autowired
    private lateinit var applicationEventPublisher: ApplicationEventPublisher

    public fun publishEvent(event: InternalEvent) {
        applicationEventPublisher.publishEvent(event)
    }

}