package io.archilab.themicroservicedungeon.gamelog.adapters.database.repositories

import io.archilab.themicroservicedungeon.gamelog.domain.dtos.*
import io.archilab.themicroservicedungeon.gamelog.domain.entities.Planet
import io.archilab.themicroservicedungeon.gamelog.domain.entities.Resource
import io.archilab.themicroservicedungeon.gamelog.domain.repositories.GameRepository
import io.archilab.themicroservicedungeon.gamelog.domain.repositories.MapRepository
import io.archilab.themicroservicedungeon.gamelog.domain.repositories.RobotRepository
import io.archilab.themicroservicedungeon.gamelog.domain.repositories.WorldRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Profile
import org.springframework.stereotype.Repository
import java.time.Instant
import java.util.*

@Repository
@Profile("!api-test")
class MapJpaRepository : MapRepository {

    @Autowired
    private lateinit var gameRepository: GameRepository

    @Autowired
    private lateinit var worldRepository: WorldRepository

    @Autowired
    private lateinit var robotRepository: RobotRepository

    private val rechargeMultiplicator = 1

    override fun getMap() : MapDto? {
        val game = gameRepository.getNewestUnfinishedGame()

        val players = game?.players?.map { it.name }?: listOf()

        val world = game?.gameWorldId?.let { worldRepository.getWorldById(it) }

        val mapPlanets = world?.planets?.let {  mapPlanetsToPlanetDtos(it) }?: listOf()

        return MapDto(game?.id, game?.gameStatus, game?.runningRound?.roundNumber, Instant.now(), players, mapPlanets)
    }

    private fun mapPlanetsToPlanetDtos(planets: Set<Planet>): List<PlanetDto> {
        return planets.map {
            planetToPlanetDto(it)
        }
    }

    private fun planetToPlanetDto(planet: Planet): PlanetDto {
        val resourceDtos = planet.resources.map {
            resourceToResourceDto(it.value)
        }
        val robotDtos = getRobotsOfPlanet(planet.id)
        return PlanetDto(
            planet.id,
            planet.x,
            planet.y,
            planet.movementDifficulty,
            rechargeMultiplicator,
            resourceDtos,
            robotDtos
        )
    }

    private fun resourceToResourceDto(resource: Resource) : ResourceDto {
        return resource.run {
            ResourceDto(resourceType, maxAmount, currentAmount)
        }
    }

    private fun getRobotsOfPlanet(planetId: UUID): List<RobotDto> {
        val robots = robotRepository.getRobotsForPlanet(planetId)
        return robots.map {
            RobotDto(it.robotId)
        }
    }

}