package io.archilab.themicroservicedungeon.gamelog.adapters.kafka.mappers

import RobotSpawnedEvent
import io.archilab.themicroservicedungeon.gamelog.domain.events.robot.embedded.Inventory
import io.archilab.themicroservicedungeon.gamelog.domain.events.robot.embedded.Planet
import io.archilab.themicroservicedungeon.gamelog.domain.events.robot.embedded.RobotProperties
import TestWithH2AndWithoutKafka
import io.archilab.themicroservicedungeon.gamelog.adapters.kafka.mocks.*
import io.archilab.themicroservicedungeon.gamelog.domain.enums.ResourceType
import io.archilab.themicroservicedungeon.gamelog.domain.enums.RestorationType
import io.archilab.themicroservicedungeon.gamelog.domain.enums.UpgradeType
import io.archilab.themicroservicedungeon.gamelog.domain.events.InternalEvent
import io.archilab.themicroservicedungeon.gamelog.domain.events.robot.*
import io.archilab.themicroservicedungeon.gamelog.domain.events.robot.embedded.*
import io.archilab.themicroservicedungeon.gamelog.domain.exceptions.MapperUnknownEventTypeException
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows
import java.time.Instant
import java.util.*

class RobotEventMapperTest : TestWithH2AndWithoutKafka() {

    private val eventMockFactory = EventMockFactory()
    private val mapper = RobotEventMapper()

    private val robotId = UUID.randomUUID()
    private val alive = true
    private val playerId = UUID.randomUUID()
    private val maxHealth = 100
    private val maxEnergy = 60
    private val energyRegeneration = 8
    private val attackDamage = 5
    private val miningSpeed = 10
    private val health = 75
    private val energy = 45
    private val healthLevel = 5
    private val damageLevel = 5
    private val miningSpeedLevel = 5
    private val miningLevel = 5
    private val energyLevel = 5
    private val energyRegenerationLevel = 5
    private val storageLevel = 5

    private val spawnPlanetMock =
        MockPlanet(UUID.randomUUID(), UUID.randomUUID(), 1, ResourceType.COAL.name.lowercase())
    private val spawnInventoryMock = MockRobotInventory(
        MockInventoryResources(0, 0, 0, 0, 0),
        0, 0, 0, true
    )

    private val robotSpawnedIntegrationEvent = eventMockFactory.createRobotSpawnedEvent(
        robotId,
        alive,
        playerId,
        maxHealth,
        maxEnergy,
        energyRegeneration,
        attackDamage,
        miningSpeed,
        health,
        energy,
        healthLevel,
        damageLevel,
        miningSpeedLevel,
        miningLevel,
        energyLevel,
        energyRegenerationLevel,
        spawnPlanetMock,
        spawnInventoryMock
    )

    @Test
    fun test_robotAttackedEvent() {
        val attacker = FightingRobotProperties(UUID.randomUUID(), 90, 90, true)
        val defender = FightingRobotProperties(UUID.randomUUID(), 70, 90, true)
        val event = eventMockFactory.createRobotAttackedEvent(attacker.id, attacker.health, attacker.energy, attacker.alive, defender.id, defender.health, defender.energy, defender.alive)
        val expected = RobotAttackedEvent(attacker, defender, Instant.parse(event.timestamp))
        assertClassTypeAndContents(event, expected, RobotAttackedEvent::class.java)
    }

    @Test
    fun test_robotMovedEvent() {
        val originPlanetId = UUID.randomUUID()
        val destinationPlanetId = UUID.randomUUID()
        val movementDifficulty = 1
        val event = eventMockFactory.createRobotMovedEvent(robotId, energy, originPlanetId, movementDifficulty, destinationPlanetId, movementDifficulty)
        val expected = RobotMovedEvent(robotId, energy, MovementPlanetProperties(originPlanetId, movementDifficulty), MovementPlanetProperties(destinationPlanetId, movementDifficulty), Instant.parse(event.timestamp))
        assertClassTypeAndContents(event, expected, RobotMovedEvent::class.java)
    }

    @Test
    fun test_robotRegeneratedEvent() {
        val event = eventMockFactory.createRobotRegeneratedEvent(robotId, energy)
        val expected = RobotRegeneratedEvent(robotId, energy, Instant.parse(event.timestamp))
        assertClassTypeAndContents(event, expected, RobotRegeneratedEvent::class.java)
    }

    @Test
    fun test_robotResourceMinedEvent() {
        val minedResource = ResourceType.COAL
        val minedAmount = 1
        val resourceInventory = ResourceInventory(minedAmount, 0, 0, 0, 0)
        val event = eventMockFactory.createRobotResourceMinedEvent(
            robotId,
            minedAmount,
            minedResource.name.lowercase(),
            resourceInventory.coal,
            resourceInventory.iron,
            resourceInventory.gem,
            resourceInventory.gold,
            resourceInventory.platin
        )
        val expected = RobotResourceMinedEvent(robotId, minedAmount, minedResource, resourceInventory, Instant.parse(event.timestamp))
        assertClassTypeAndContents(event, expected, RobotResourceMinedEvent::class.java)
    }

    @Test
    fun test_robotResourceRemovedEvent() {
        val removedResource = ResourceType.COAL
        val removedAmount = 1
        val resourceInventory = ResourceInventory(removedAmount, 0, 0, 0, 0)
        val event = eventMockFactory.createRobotResourceRemovedEvent(
            robotId,
            removedAmount,
            removedResource.name.lowercase(),
            resourceInventory.coal,
            resourceInventory.iron,
            resourceInventory.gem,
            resourceInventory.gold,
            resourceInventory.platin
        )
        val expected = RobotResourceRemovedEvent(robotId, removedAmount, removedResource, resourceInventory, Instant.parse(event.timestamp))
        assertClassTypeAndContents(event, expected, RobotResourceRemovedEvent::class.java)
    }

    @Test
    fun test_robotRestoredAttributesEvent() {
        val restorationType = RestorationType.HEALTH
        val availableEnergy = 10
        val availableHealth = 50
        val event = eventMockFactory.createRobotRestoredAttributesEvent(
            robotId,
            restorationType.type,
            availableEnergy,
            availableHealth
        )
        val expected = RobotRestoredAttributesEvent(robotId, restorationType, availableEnergy, availableHealth)
        assertClassTypeAndContents(event, expected, RobotRestoredAttributesEvent::class.java)
    }

    @Test
    fun test_robotSpawnedEvent() {
        val expected = RobotSpawnedEvent(
            RobotProperties(
                robotId,
                alive,
                playerId,
                maxHealth,
                maxEnergy,
                energyRegeneration,
                attackDamage,
                miningSpeed,
                health,
                energy,
                healthLevel,
                damageLevel,
                miningSpeedLevel,
                miningLevel,
                energyLevel,
                energyRegenerationLevel,
                spawnPlanetMock.let {
                    Planet(it.planetId, it.gameWorldId, it.movementDifficulty, it.resourceType)
                },
                spawnInventoryMock.let {
                    Inventory(
                        ResourceInventory(it.resources.coal, it.resources.iron, it.resources.gem, it.resources.gold, it.resources.platin),
                        it.storageLevel, it.usedStorage, it.maxStorage, it.full)
                }
            )
        )
        assertClassTypeAndContents(robotSpawnedIntegrationEvent, expected, RobotSpawnedEvent::class.java)
    }

    @Test
    fun test_robotUpgradedEvent() {
        val level = 1
        val upgrade = UpgradeType.STORAGE
        val event = eventMockFactory.createRobotUpgradedEvent(
            robotId,
            level,
            upgrade.type,
            alive,
            playerId,
            maxHealth,
            maxEnergy,
            energyRegeneration,
            attackDamage,
            miningSpeed,
            health,
            energy,
            healthLevel,
            damageLevel,
            miningSpeedLevel,
            miningLevel,
            energyLevel,
            energyRegenerationLevel,
            spawnPlanetMock,
            spawnInventoryMock
        )
        val robot = RobotProperties(
            robotId,
            alive,
            playerId,
            maxHealth,
            maxEnergy,
            energyRegeneration,
            attackDamage,
            miningSpeed,
            health,
            energy,
            healthLevel,
            damageLevel,
            miningSpeedLevel,
            miningLevel,
            energyLevel,
            energyRegenerationLevel,
            spawnPlanetMock.let {
                Planet(it.planetId, it.gameWorldId, it.movementDifficulty, it.resourceType)
            },
            spawnInventoryMock.let {
                Inventory(
                    ResourceInventory(it.resources.coal, it.resources.iron, it.resources.gem, it.resources.gold, it.resources.platin),
                    it.storageLevel, it.usedStorage, it.maxStorage, it.full)
            }
        )
        val expected = RobotUpgradedEvent(robotId, level, upgrade, robot)
        assertClassTypeAndContents(event, expected, RobotUpgradedEvent::class.java)
    }

    @Test
    fun test_robotRevealedEvent() {
        val planetId = UUID.randomUUID()
        val playerNotion = playerId.toString().substring(0 until 4)
        val event = eventMockFactory.createRobotsRevealedEvent(
            robotId,
            planetId,
            playerNotion,
            health,
            energy,
            healthLevel,
            damageLevel,
            miningSpeedLevel,
            miningLevel,
            energyLevel,
            energyRegenerationLevel,
            storageLevel
        )
        val expected = RobotsRevealedEvent(
            listOf(
                RevealedRobotProperties(
                    robotId,
                    planetId,
                    playerNotion,
                    health,
                    energy,
                    RobotLevels(
                        healthLevel,
                        damageLevel,
                        miningSpeedLevel,
                        miningLevel,
                        energyLevel,
                        energyRegenerationLevel,
                        storageLevel
                    )
                )
            )
        )

        assertClassTypeAndContents(event, expected, RobotsRevealedEvent::class.java)
    }

    @Test
    fun test_exception() {
        assertThrows<MapperUnknownEventTypeException> {
            mapper.mapConsumerRecordToEvent("foobar", robotSpawnedIntegrationEvent.timestamp, robotSpawnedIntegrationEvent.payload)
        }
    }

    private fun assertClassTypeAndContents(event: EventMock, expected: InternalEvent, eventClass: Class<out InternalEvent>) {
        val result = mapper.mapConsumerRecordToEvent(event.type, event.timestamp, event.payload)
        assertThat(result.javaClass).isEqualTo(eventClass)
        assertThat(result).usingRecursiveComparison().isEqualTo(expected)
    }

}
