package io.archilab.themicroservicedungeon.gamelog.adapters.api

import io.archilab.themicroservicedungeon.gamelog.domain.dtos.ScoreboardTrophiesDto
import io.archilab.themicroservicedungeon.gamelog.domain.repositories.ScoreboardTrophiesRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.CrossOrigin
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@CrossOrigin
@RequestMapping("scoreboard-trophies")
class ScoreboardTrophiesApi {

    @Autowired
    private lateinit var scoreboardTrophiesRepository: ScoreboardTrophiesRepository

    @GetMapping(produces = ["application/json"])
    fun getScoreboard(): ResponseEntity<ScoreboardTrophiesDto> {
        val scoreboardTrophies = scoreboardTrophiesRepository.getScoreboardTrophies()
        return ResponseEntity(scoreboardTrophies, HttpStatus.OK)
    }

}