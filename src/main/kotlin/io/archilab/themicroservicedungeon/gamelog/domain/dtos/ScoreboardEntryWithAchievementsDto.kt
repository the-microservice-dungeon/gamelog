package io.archilab.themicroservicedungeon.gamelog.domain.dtos

import com.fasterxml.jackson.annotation.JsonIgnoreProperties

data class ScoreboardEntryWithAchievementsDto(
    val player: PlayerDto,
    val totalScore: Int,
    val fightingScore:Int,
    val miningScore:Int,
    val tradingScore: Int,
    val travelingScore: Int,
    @JsonIgnoreProperties("player")
    val achievements: List<PlayerAchievementDto>
)