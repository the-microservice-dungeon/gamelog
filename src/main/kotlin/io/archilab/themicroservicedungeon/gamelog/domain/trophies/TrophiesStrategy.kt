package io.archilab.themicroservicedungeon.gamelog.domain.trophies

import io.archilab.themicroservicedungeon.gamelog.domain.entities.Game
import io.archilab.themicroservicedungeon.gamelog.domain.entities.PlayerScore
import io.archilab.themicroservicedungeon.gamelog.domain.enums.ScoreboardCategory
import io.archilab.themicroservicedungeon.gamelog.domain.enums.TrophyType
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component

@Component
class TrophiesStrategy() {

    @Autowired
    private lateinit var trophiesManager: TrophiesManager

    fun awardTrophiesForGame(game: Game) {
        awardFightingTrophies(game)
        awardMiningTrophies(game)
        awardTradingTrophies(game)
        awardTravelingTrophies(game)
        awardGameTrophies(game)
    }

    private fun awardFightingTrophies(game: Game) {
        awardByCategory(
            game,
            ScoreboardCategory.FIGHTING,
            TrophyType.FightingFirstPlace,
            TrophyType.FightingSecondPlace,
            TrophyType.FightingThirdPlace
        )
    }

    private fun awardMiningTrophies(game: Game) {
        awardByCategory(
            game,
            ScoreboardCategory.MINING,
            TrophyType.MiningFirstPlace,
            TrophyType.MiningSecondPlace,
            TrophyType.MiningThirdPlace
        )
    }

    private fun awardTradingTrophies(game: Game) {
        awardByCategory(
            game,
            ScoreboardCategory.TRADING,
            TrophyType.TradingFirstPlace,
            TrophyType.TradingSecondPlace,
            TrophyType.TradingThirdPlace
        )
    }

    private fun awardTravelingTrophies(game: Game) {
        awardByCategory(
            game,
            ScoreboardCategory.TRAVELING,
            TrophyType.TravelingFirstPlace,
            TrophyType.TravelingSecondPlace,
            TrophyType.TravelingThirdPlace
        )
    }

    private fun awardGameTrophies(game: Game) {
        awardByCategory(
            game,
            ScoreboardCategory.GAME,
            TrophyType.GameFirstPlace,
            TrophyType.GameSecondPlace,
            TrophyType.GameThirdPlace
        )
    }

    private fun awardByCategory(game: Game, category: ScoreboardCategory, firstPlace: TrophyType, secondPlace: TrophyType, thirdPlace: TrophyType) {
        val orderedScores = orderScoresRemovingDuplicates(game.scoreboard.playerScores, category)
        val highestScore = orderedScores.getOrNull(0)?: -1
        val secondBestScore = orderedScores.getOrNull(1)?: -1
        val thirdBestScore = orderedScores.getOrNull(2)?: -1
        game.scoreboard.playerScores.forEach { playerScore ->
            when (getScoreForCategory(playerScore, category)) {
                highestScore -> trophiesManager.getTrophy(firstPlace)
                secondBestScore -> trophiesManager.getTrophy(secondPlace)
                thirdBestScore -> trophiesManager.getTrophy(thirdPlace)
                else -> null
            }?.also {
                trophy -> playerScore.player.awardTrophy(game.id, trophy)
            }
        }
    }

    private fun orderScoresRemovingDuplicates(playerScores: List<PlayerScore>, category: ScoreboardCategory): List<Int> {
        return playerScores.map { playerScore -> getScoreForCategory(playerScore, category) }.distinct().sortedDescending()
    }

    private fun getScoreForCategory(playerScore: PlayerScore, category: ScoreboardCategory): Int {
        return when (category) {
            ScoreboardCategory.FIGHTING -> playerScore.fightingScore
            ScoreboardCategory.GAME -> playerScore.totalScore
            ScoreboardCategory.MINING -> playerScore.miningScore
            ScoreboardCategory.TRADING -> playerScore.tradingScore
            ScoreboardCategory.TRAVELING -> playerScore.travelingScore
        }
    }

}