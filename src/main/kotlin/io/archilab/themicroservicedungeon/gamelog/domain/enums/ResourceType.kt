package io.archilab.themicroservicedungeon.gamelog.domain.enums

import com.fasterxml.jackson.annotation.JsonAlias

enum class ResourceType () {
    @JsonAlias("coal")
    COAL,
    @JsonAlias("iron")
    IRON,
    @JsonAlias("gem")
    GEM,
    @JsonAlias("gold")
    GOLD,
    @JsonAlias("platin")
    PLATIN
}