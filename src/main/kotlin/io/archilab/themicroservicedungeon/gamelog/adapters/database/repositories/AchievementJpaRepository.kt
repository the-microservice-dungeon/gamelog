package io.archilab.themicroservicedungeon.gamelog.adapters.database.repositories

import io.archilab.themicroservicedungeon.gamelog.adapters.database.interfaces.AchievementCrudRepository
import io.archilab.themicroservicedungeon.gamelog.domain.entities.Achievement
import io.archilab.themicroservicedungeon.gamelog.domain.enums.AchievementType
import io.archilab.themicroservicedungeon.gamelog.domain.repositories.AchievementRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.repository.findByIdOrNull
import org.springframework.stereotype.Repository

@Repository
class AchievementJpaRepository : AchievementRepository {

    @Autowired
    private lateinit var achievementCrudRepository: AchievementCrudRepository

    override fun store(achievement: Achievement) {
        achievementCrudRepository.save(achievement)
    }

    override fun retrieve(achievementType: AchievementType): Achievement? {
        return achievementCrudRepository.findByIdOrNull(achievementType.id)
    }

}