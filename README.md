# Gamelog Version 2

This is the new rewrite of the Gamelog service.

The old Gamelog was deprecated because the changes in the microservice landscape, event architecture and tasks of the Gamelog service defined in Microservice Dungeon 3.0 require vast changes that justify doing a rewrite instead.  
If you want to get to the old Gamelog, have a look at the [ArchivedGamelogV1 branch](https://gitlab.com/the-microservice-dungeon/gamelog/-/tree/ArchivedGamelogV1).

# Inner structure
The inner structure of the service is made up by its package structure.

There is a general separation between the domain/business logic of the service (e.g. analysis of events, awarding of achievements and trophies, calculation of player scores) and the adapters that connect the service to the supporting infrastructure (e.g. Kafka, a database system, etc.). The general idea is that the domain package contains the hard core of the service, whereas the parts making up the adapters package could be replaced with other fitting parts to enable migrating to a different infrasture landscape (e.g. other database system or a different message broker).

The following diagram illustrates the general separation of the system parts.  
As you can see, the `events` package belongs to the domain logic, as the raw Kafka events are mapped into domain events that are then handled by the domain logic. This would enable replacing Kafka with a different message broker system by providing a different set of mappers, while not having to touch the domain logic at all.

```plantuml
@startuml
package "io.archilab.themicroservicedungeon.gamelog" <<Rectangle>> #DDDDDD {
    package "adapters" <<Rectangle>> #DDDDDD {
        package "kafka" <<Rectangle>> #DDDDDD {
            package "listeners" <<Rectangle>> #DDDDDD {
            }
            package "mappers" <<Rectangle>> #DDDDDD {
            }
        }
        package "database" <<Rectangle>> #DDDDDD {
        }
    }
    package "domain" <<Rectangle>> #DDDDDD {
        package "events" <<Rectangle>> #DDDDDD {
            package "game" <<Rectangle>> #DDDDDD {
            }
            package "map" <<Rectangle>> #DDDDDD {
            }
            package "robot" <<Rectangle>> #DDDDDD {
            }
            package "trading" <<Rectangle>> #DDDDDD {
            }
        }
        package "exceptions" <<Rectangle>> #DDDDDD {
        }
    }
}
@enduml
```

# Testing against the APIs
Normally, the Gamelog requires the whole Microservice Dungeon service landscape (as provided by e.g. the local-dev-environment).  
For testing against the Gamelog APIs, you can instead make use of the `api-test` profile when running the application. This disables the Kafka listeners and makes the Gamelog use a H2 in-memory database instead of the database server of the service landscape.  
When using the profile, the APIs will provide mock data instead of live data, which may also be easier for testing.