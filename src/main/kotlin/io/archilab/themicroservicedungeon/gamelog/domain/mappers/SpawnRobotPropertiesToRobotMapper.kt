package io.archilab.themicroservicedungeon.gamelog.domain.mappers

import io.archilab.themicroservicedungeon.gamelog.domain.entities.Inventory
import io.archilab.themicroservicedungeon.gamelog.domain.events.robot.embedded.RobotProperties
import io.archilab.themicroservicedungeon.gamelog.domain.entities.Robot

class SpawnRobotPropertiesToRobotMapper {

    fun mapRobotPropertiesToRobot(robotProperties: RobotProperties): Robot {
        val robot = with (robotProperties) {
            Robot(this.robotId, this.alive, this.playerId, this.maxHealth, this.maxEnergy, this.energyRegeneration, this.attackDamage,
                this.miningSpeed, this.health, this.energy, this.healthLevel, this.damageLevel, this.miningSpeedLevel, this.miningLevel,
                this.energyLevel, this.energyRegenerationLevel, this.inventory.storageLevel).apply {
                spawn(robotProperties.planet.planetId)
                inventory = robotProperties.inventory.resources.let { Inventory(it.coal, it.iron, it.gem, it.gold, it.platin) }
            }
        }
        return robot
    }

}