package io.archilab.themicroservicedungeon.gamelog.domain.entities

import io.archilab.themicroservicedungeon.gamelog.domain.enums.ResourceType
import javax.persistence.Column
import javax.persistence.Embeddable

@Embeddable
class Resource() {

    @Column
    lateinit var resourceType: ResourceType

    @Column
    var maxAmount: Int = 0

    @Column
    var currentAmount: Int = 0

    constructor(resourceType: ResourceType, maxAmount: Int, currentAmount: Int) : this() {
        this.resourceType = resourceType
        this.maxAmount = maxAmount
        this.currentAmount = currentAmount
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as Resource

        if (resourceType != other.resourceType) return false
        if (maxAmount != other.maxAmount) return false
        if (currentAmount != other.currentAmount) return false

        return true
    }

    override fun hashCode(): Int {
        var result = resourceType.hashCode()
        result = 31 * result + maxAmount
        result = 31 * result + currentAmount
        return result
    }

}