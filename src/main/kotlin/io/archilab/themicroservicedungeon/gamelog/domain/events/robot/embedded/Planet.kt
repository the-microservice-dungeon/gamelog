package io.archilab.themicroservicedungeon.gamelog.domain.events.robot.embedded

import java.util.*

data class Planet (
    val planetId: UUID,
    val gameWorldId: UUID,
    val movementDifficulty: Int,
    val resourceType: String?
)