package io.archilab.themicroservicedungeon.gamelog.domain.entities

import io.archilab.themicroservicedungeon.gamelog.domain.enums.ResourceType
import org.hibernate.annotations.Type
import java.util.*
import javax.persistence.Column
import javax.persistence.ElementCollection
import javax.persistence.Entity
import javax.persistence.Id

@Entity
class Planet() {

    @Id
    @Type(type = "uuid-char")
    lateinit var id: UUID

    @Column
    var x: Int = 0

    @Column
    var y: Int = 0

    @Column
    var movementDifficulty: Int = 0

    @ElementCollection
    lateinit var resources: Map<ResourceType, Resource>

    constructor(
        id: UUID,
        x: Int,
        y: Int,
        movementDifficulty: Int,
        resources: Map<ResourceType, Resource>
    ) : this() {
        this.id = id
        this.x = x
        this.y = y
        this.movementDifficulty = movementDifficulty
        this.resources = resources
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as Planet

        if (id != other.id) return false
        if (x != other.x) return false
        if (y != other.y) return false
        if (movementDifficulty != other.movementDifficulty) return false
        if (resources != other.resources) return false

        return true
    }

    override fun hashCode(): Int {
        var result = id.hashCode()
        result = 31 * result + x
        result = 31 * result + y
        result = 31 * result + movementDifficulty
        result = 31 * result + resources.hashCode()
        return result
    }


}