package io.archilab.themicroservicedungeon.gamelog.adapters.kafka.mappers

import TestWithH2AndWithoutKafka
import io.archilab.themicroservicedungeon.gamelog.domain.exceptions.MapperUnknownEventTypeException
import io.archilab.themicroservicedungeon.gamelog.domain.events.map.ResourceMinedEvent
import io.archilab.themicroservicedungeon.gamelog.domain.events.map.embedded.GameWorldResource
import io.archilab.themicroservicedungeon.gamelog.domain.enums.ResourceType
import io.archilab.themicroservicedungeon.gamelog.adapters.kafka.mocks.EventMockFactory
import io.archilab.themicroservicedungeon.gamelog.domain.enums.Direction
import io.archilab.themicroservicedungeon.gamelog.domain.events.map.NeighbourPlanet
import io.archilab.themicroservicedungeon.gamelog.domain.events.map.PlanetDiscoveredEvent
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows
import java.util.*

class PlanetEventMapperTest : TestWithH2AndWithoutKafka() {

    private val eventMockFactory = EventMockFactory()
    private val mapper = PlanetEventMapper()


    private val planetId = UUID.randomUUID()
    private val movementDifficulty = 1
    private val neighbourPlanetId = UUID.randomUUID()
    private val neighbourPlanetDirection = Direction.NORTH
    private val defaultNumber = 0
    private val resourceType = ResourceType.COAL
    private val resourceMaxAmount = 100
    private val resourceCurrentAmount = 80
    private val resourceMinedEvent = eventMockFactory.createResourceMinedEventMock(
        planetId,
        defaultNumber,
        resourceType.name,
        defaultNumber,
        defaultNumber
    )
    private val planetDiscoveredEvent = eventMockFactory.createPlanetDiscoveredEventMock(
        planetId,
        movementDifficulty, neighbourPlanetId, neighbourPlanetDirection.direction, resourceMaxAmount, resourceCurrentAmount, resourceType.name
    )

    @Test
    fun test_resourceMined() {
        val expected =
            ResourceMinedEvent(planetId, defaultNumber, GameWorldResource(resourceType, defaultNumber, defaultNumber))
        val result = mapper.mapConsumerRecordToEvent(resourceMinedEvent.type, resourceMinedEvent.timestamp, resourceMinedEvent.payload)
        assertThat(result.javaClass).isEqualTo(ResourceMinedEvent::class.java)
        assertThat(result).isEqualTo(expected)
    }

    @Test
    fun test_planetDiscovered() {
        val expected = PlanetDiscoveredEvent(planetId, movementDifficulty, listOf(NeighbourPlanet(neighbourPlanetId, neighbourPlanetDirection)), GameWorldResource(resourceType, resourceMaxAmount, resourceCurrentAmount))
        val result = planetDiscoveredEvent.let { mapper.mapConsumerRecordToEvent(it.type, it.timestamp, it.payload) }
        assertThat(result.javaClass).isEqualTo(PlanetDiscoveredEvent::class.java)
        assertThat(result).isEqualTo(expected)
    }

    @Test
    fun test_exception() {
        assertThrows<MapperUnknownEventTypeException> {
            mapper.mapConsumerRecordToEvent("foobar", resourceMinedEvent.timestamp, resourceMinedEvent.payload)
        }
    }

}