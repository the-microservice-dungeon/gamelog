package io.archilab.themicroservicedungeon.gamelog.domain.achievements

import TestWithH2AndWithoutKafka
import io.archilab.themicroservicedungeon.gamelog.domain.entities.PlayerAchievement
import io.archilab.themicroservicedungeon.gamelog.domain.enums.AchievementType
import io.archilab.themicroservicedungeon.gamelog.domain.factories.PreconfiguredPlayersFactory
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import java.util.*

class AchievementsStrategyTest: TestWithH2AndWithoutKafka() {

    private val gameId = UUID.randomUUID()

    @Autowired
    private lateinit var achievementsManager: AchievementsManager

    @Autowired
    private lateinit var achievementsStrategy: AchievementsStrategy

    private val preconfiguredPlayersFactory = PreconfiguredPlayersFactory()

    @Test
    fun test_fighting() {
        testForAchievement(AchievementType.FIGHTING_BRONZE)
        testForAchievement(AchievementType.FIGHTING_SILVER)
        testForAchievement(AchievementType.FIGHTING_GOLD)
    }

    @Test
    fun test_mining() {
        testForAchievement(AchievementType.MINING_BRONZE)
        testForAchievement(AchievementType.MINING_SILVER)
        testForAchievement(AchievementType.MINING_GOLD)
    }

    @Test
    fun test_trading() {
        testForAchievement(AchievementType.TRADING_BRONZE)
        testForAchievement(AchievementType.TRADING_SILVER)
        testForAchievement(AchievementType.TRADING_GOLD)
    }

    @Test
    fun test_traveling() {
        testForAchievement(AchievementType.TRAVELING_BRONZE)
        testForAchievement(AchievementType.TRAVELING_SILVER)
        testForAchievement(AchievementType.TRAVELING_GOLD)
    }

    private fun testForAchievement(achievementType: AchievementType) {
        val (notAchieving, achieving) = preconfiguredPlayersFactory.createAchievingPlayerPair(achievementType)

        achievementsStrategy.awardAchievementsToPlayer(gameId, notAchieving)
        achievementsStrategy.awardAchievementsToPlayer(gameId, achieving)

        val achievement = achievementsManager.getAchievement(achievementType)!!
        assertThat(notAchieving.achievements).usingRecursiveFieldByFieldElementComparator().doesNotContain(PlayerAchievement(gameId, achievement))
        assertThat(achieving.achievements).usingRecursiveFieldByFieldElementComparator()
            .contains(PlayerAchievement(gameId, achievement))
    }

}