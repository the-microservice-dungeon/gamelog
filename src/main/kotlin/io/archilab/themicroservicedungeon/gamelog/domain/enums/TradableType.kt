package io.archilab.themicroservicedungeon.gamelog.domain.enums

import com.fasterxml.jackson.annotation.JsonValue

enum class TradableType (@get:JsonValue val type: String) {
    ITEM("ITEM"),
    UPGRADE("UPGRADE"),
    RESTORATION("RESTORATION"),
    RESOURCE("RESOURCE")
}