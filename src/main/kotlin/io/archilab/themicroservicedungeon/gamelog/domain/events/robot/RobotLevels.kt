package io.archilab.themicroservicedungeon.gamelog.domain.events.robot

import com.fasterxml.jackson.annotation.JsonProperty

data class RobotLevels (
    @JsonProperty("healthLevel")
    val healthLevel: Int,
    @JsonProperty("damageLevel")
    val damageLevel: Int,
    @JsonProperty("miningSpeedLevel")
    val miningSpeedLevel: Int,
    @JsonProperty("miningLevel")
    val miningLevel: Int,
    @JsonProperty("energyLevel")
    val energyLevel: Int,
    @JsonProperty("energyRegenLevel")
    val energyRegenLevel: Int,
    @JsonProperty("storageLevel")
    val storageLevel: Int
)