package io.archilab.themicroservicedungeon.gamelog.domain.entities

import javax.persistence.Entity
import javax.persistence.Id

@Entity
class Trophy() {

    @Id
    var id: Int = -1
    lateinit var name: String
    lateinit var imageUrl: String

    constructor(id: Int, name: String, imageUrl: String): this()
    {
        this.id = id
        this.name = name
        this.imageUrl = imageUrl
    }
}