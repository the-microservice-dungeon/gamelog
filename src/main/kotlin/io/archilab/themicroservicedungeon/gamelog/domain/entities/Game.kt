package io.archilab.themicroservicedungeon.gamelog.domain.entities

import io.archilab.themicroservicedungeon.gamelog.domain.enums.GameStatus
import io.archilab.themicroservicedungeon.gamelog.domain.enums.RoundStatus
import org.hibernate.annotations.Type
import java.time.Instant
import java.util.*
import javax.persistence.*

@Entity
class Game() {

    @Id
    @Column
    @Type(type = "uuid-char")
    lateinit var id: UUID

    @Column
    @Type(type = "uuid-char")
    var gameWorldId: UUID? = null

    @Column
    lateinit var gameStatus: GameStatus

    @Column
    var startedAt: Instant? = null
        private set

    @Column
    var endedAt: Instant? = null
        private set

    @OneToMany(cascade = [CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH])
    lateinit var rounds: Set<Round>

    @ManyToMany(mappedBy = "games")
    lateinit var players: MutableList<Player>

    @Embedded
    lateinit var scoreboard: Scoreboard

    val runningRound: Round?
        get() {
            val newestRound = rounds.maxByOrNull { round -> round.roundNumber }
            return newestRound.takeIf { round -> round?.roundStatus != RoundStatus.ENDED }
        }

    constructor(id: UUID, gameWorldId: UUID?, gameStatus: GameStatus) : this() {
        this.id = id
        this.gameWorldId = gameWorldId
        this.gameStatus = gameStatus
        rounds = setOf()
        players = mutableListOf()
        scoreboard = Scoreboard(id, 0, listOf())
    }

    fun startGame(startedAt: Instant) {
        gameStatus = GameStatus.STARTED
        this.startedAt = startedAt
    }

    fun endGame(endedAt: Instant) {
        gameStatus = GameStatus.ENDED
        this.endedAt = endedAt
    }

    fun newRound(roundId: UUID, roundNumber: Int, startedAt: Instant) {
        val round = Round(id, roundId, roundNumber, RoundStatus.STARTED, startedAt)
        this.rounds = rounds.plus(round)
    }

    fun updateRound(roundId: UUID, roundStatus: RoundStatus, timestamp: Instant) {
        rounds.find { round -> round.roundId == roundId }?.apply {
            if (roundStatus == RoundStatus.COMMAND_INPUT_ENDED) {
                commandInputEnded(timestamp)
            }
            if (roundStatus == RoundStatus.ENDED) {
                roundEnded(timestamp)
            }
        }
    }

    fun registerPlayer(player: Player) {
        players.add(player)
    }
}