package io.archilab.themicroservicedungeon.gamelog.domain.entities

import javax.persistence.Column
import javax.persistence.Embeddable

@Embeddable
class Inventory () {

    @Column(name = "inventory_coal")
    var coal: Int = 0

    @Column(name = "inventory_iron")
    var iron: Int = 0

    @Column(name = "inventory_gem")
    var gem: Int = 0

    @Column(name = "inventory_gold")
    var gold: Int = 0

    @Column(name = "inventory_platin")
    var platin: Int = 0


    constructor(coal : Int, iron : Int, gem : Int, gold : Int, platin : Int) : this() {
        this.coal = coal
        this.iron = iron
        this.gem = gem
        this.gold = gold
        this.platin = platin
    }

}