package io.archilab.themicroservicedungeon.gamelog.domain.enums

enum class PlayerSuccess {
    LOW,
    MEDIUM,
    HIGH
}