package io.archilab.themicroservicedungeon.gamelog.adapters.kafka.mappers

import com.fasterxml.jackson.core.exc.StreamReadException
import com.fasterxml.jackson.databind.DatabindException
import com.fasterxml.jackson.databind.InjectableValues
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.kotlin.registerKotlinModule
import java.io.IOException

class InjectingObjectMapper : ObjectMapper() {

    init {
        registerKotlinModule()
    }

    @Throws(IOException::class, StreamReadException::class, DatabindException::class)
    fun <T> mapInjectingValues(valuesToInject: Map<String, Any>, jsonDocument: ByteArray?, targetClass: Class<T>?): T {
        val injectableValues = InjectableValues.Std().apply {
            valuesToInject.forEach { (key, value) -> addValue(key, value) }
        }
        return this.reader(injectableValues).readValue(jsonDocument, targetClass)
    }

}