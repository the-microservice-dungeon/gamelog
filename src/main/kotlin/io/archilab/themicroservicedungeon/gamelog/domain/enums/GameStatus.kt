package io.archilab.themicroservicedungeon.gamelog.domain.enums

import com.fasterxml.jackson.annotation.JsonValue

enum class GameStatus (@get:JsonValue val status: String) {
    CREATED("created"),
    STARTED("started"),
    ENDED("ended")
}